#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

#include <iostream>

using namespace cv;
using namespace std;

void mainb() {

    VideoCapture cap(0);
    Mat img, imgGray, padded, complexI, magI;
    int m, n;

    while (true) {

        cap.read(img);
        cvtColor(img, imgGray, COLOR_BGR2GRAY);


        //! [expand]
       // Mat padded;                            //expand input image to optimal size
        m = getOptimalDFTSize(imgGray.rows);
        n = getOptimalDFTSize(imgGray.cols); // on the border add zero values
        copyMakeBorder(imgGray, padded, 0, m - imgGray.rows, 0, n - imgGray.cols, BORDER_CONSTANT, Scalar::all(0));
        //! [expand]

        //! [complex_and_real]
        Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
        //Mat complexI;
        merge(planes, 2, complexI);         // Add to the expanded another plane with zeros
       //! [complex_and_real]

       //! [dft]
        dft(complexI, complexI);            // this way the result may fit in the source matrix
       //! [dft]

           // compute the magnitude and switch to logarithmic scale
           // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
       //! [magnitude]
        split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
        magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
        magI = planes[0];
        //! [magnitude]

        //! [log]
        magI += Scalar::all(1);                    // switch to logarithmic scale
        log(magI, magI);
        //! [log]

        //! [crop_rearrange]
            // crop the spectrum, if it has an odd number of rows or columns
        magI = magI(Rect(0, 0, magI.cols & -2, magI.rows & -2));

        // rearrange the quadrants of Fourier image  so that the origin is at the image center
        int cx = magI.cols / 2;
        int cy = magI.rows / 2;

        Mat q0(magI, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
        Mat q1(magI, Rect(cx, 0, cx, cy));  // Top-Right
        Mat q2(magI, Rect(0, cy, cx, cy));  // Bottom-Left
        Mat q3(magI, Rect(cx, cy, cx, cy)); // Bottom-Right

        Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
        q0.copyTo(tmp);
        q3.copyTo(q0);
        tmp.copyTo(q3);

        q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
        q2.copyTo(q1);
        tmp.copyTo(q2);
        //! [crop_rearrange]

        //! [normalize]
        normalize(magI, magI, 0, 1, NORM_MINMAX); // Transform the matrix with float values into a
                                                // viewable image form (float between values 0 and 1).
       //! [normalize]

        imshow("Input Image", imgGray);    // Show the result
        imshow("spectrum magnitude", magI);


        //imshow("Image", imgGray);
        waitKey(1);
    }
}