// ACGTut3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

constexpr auto WINDOW_NAME = "Display Window";

int main(int argc, char** argv)
{
    cv::VideoCapture capture(0);
    cv::Mat image;

    cv::namedWindow(WINDOW_NAME, cv::WINDOW_AUTOSIZE); // Create a window for display.

    //// Read from file
    //image = cv::imread("..\\..\\Resources\\lambo.png", cv::IMREAD_COLOR); // Read the file
    //if (image.empty()) // Check for invalid input
    //{
    //    std::cout << "Could not open or find the image" << std::endl;
    //    return -1;
    //}
    //cv::imshow(WINDOW_NAME, image);

    while (true) {
        // Read from the camera
        capture.read(image);
        cv::imshow(WINDOW_NAME, image);

        // Wait for enter or space key pressed
        const auto key = cv::waitKey(16);
        if (key == 13 || key == 32) {
            break;
        }
    }

    return 0;
}
