// FaceDetection.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>

constexpr auto WINDOW_NAME = "Display Window";

int main(int argc, char** argv)
{
    cv::VideoCapture capture(0);
    cv::Mat image;

    cv::namedWindow(WINDOW_NAME, cv::WINDOW_AUTOSIZE); // Create a window for display.

    cv::CascadeClassifier faceCascade, plateCascade;
    faceCascade.load("..\\..\\Resources\\haarcascade_frontalface_default.xml");
    plateCascade.load("..\\..\\Resources\\haarcascade_russian_plate_number.xml");

    if (faceCascade.empty()) {
        std::cout << "Classifier not loaded." << std::endl;
        return 1;
    }

    std::vector<cv::Rect> faces, plates;
    while (true) {
        // Read from the camera
        capture.read(image);

        faces.clear();
        plates.clear();

        faceCascade.detectMultiScale(image, faces, 1.1, 10);
        plateCascade.detectMultiScale(image, plates, 1.1, 10);

        for (auto face : faces) {
            cv::rectangle(image, face.tl(), face.br(), cv::Scalar(255, 0, 255), 3);
        }
        for (auto plate : plates) {
            cv::rectangle(image, plate.tl(), plate.br(), cv::Scalar(0, 255, 0), 3);
        }

        cv::imshow(WINDOW_NAME, image);

        // Wait for enter or space key pressed
        const auto key = cv::waitKey(1);
        if (key == 13 || key == 32) {
            break;
        }
    }

    return 0;
}
