#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat resize;

	cv::namedWindow("Input");
	cv::namedWindow("Resize");


	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		cv::imshow("Input", frame);

		//New height is 70% and new width is 50% of the size of orignial image
		int newHeight = 0.7*frame.rows;
		int newWidth = 0.5*frame.cols;
		cv::resize(frame, resize, cv::Size(newWidth, newHeight));
		cv::imshow("Resize", resize);


		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}