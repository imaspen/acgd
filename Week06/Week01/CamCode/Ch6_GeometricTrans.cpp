#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

/// Global variables
const char* source_window = "Source image";
const char* warp_window = "Warp";
const char* warp_rotate_window = "Warp + Rotate";

int main()
{
	cv::Point2f srcTri[3];
	cv::Point2f dstTri[3];

	cv::Mat rot_mat(2, 3, CV_32FC1);
	cv::Mat warp_mat(2, 3, CV_32FC1);
	cv::Mat src, warp_dst, warp_rotate_dst;


	/// Load the webcam
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}
	bool stop = false;
	int delay = 30; //ms

	
	while (!stop)
	{
		if (!webcam.read(src))
			break;

		/// Set the dst image the same type and size as src
		warp_dst = cv::Mat::zeros(src.rows, src.cols, src.type());

		/// Set your 3 points to calculate the  Affine Transform
		srcTri[0] = cv::Point2f(0, 0);
		srcTri[1] = cv::Point2f(src.cols - 1.f, 0);
		srcTri[2] = cv::Point2f(0, src.rows - 1.f);

		dstTri[0] = cv::Point2f(src.cols*0.0f, src.rows*0.33f);
		dstTri[1] = cv::Point2f(src.cols*0.85f, src.rows*0.25f);
		dstTri[2] = cv::Point2f(src.cols*0.15f, src.rows*0.7f);

		/// Get the Affine Transform
		warp_mat = getAffineTransform(srcTri, dstTri);

		/// Apply the Affine Transform just found to the src image
		warpAffine(src, warp_dst, warp_mat, warp_dst.size());

		/** Rotating the image after Warp */

		/// Compute a rotation matrix with respect to the center of the image
		cv::Point center = cv::Point(warp_dst.cols / 2, warp_dst.rows / 2);
		double angle = -50.0;
		double scale = 0.6;

		/// Get the rotation matrix with the specifications above
		rot_mat = cv::getRotationMatrix2D(center, angle, scale);

		/// Rotate the warped image
		warpAffine(warp_dst, warp_rotate_dst, rot_mat, warp_dst.size());


		/// Show what you got
		
		imshow("Image Input", src);

     	imshow("Warp", warp_dst);

		imshow("Warp + Rotate", warp_rotate_dst);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}

	return 0;
}
