#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	cv::Mat src ;
	cv::Mat polarImg;

	bool stop = false;
	int delay = 30; //ms


	while (!stop)
	{
		if (!webcam.read(src))
			break;
		
		cv::linearPolar(src, polarImg, cv::Point(src.cols/2, src.rows / 2), src.rows/2 ,cv::INTER_CUBIC);
		cv::imshow("Image Window", polarImg);
		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();

	return 0;  
}