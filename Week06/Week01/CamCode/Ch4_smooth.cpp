#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat gaussian;
	cv::Mat blur;
	cv::Mat median;
	cv::Mat bilateral;

	cv::namedWindow("Input");
	cv::namedWindow("Gaussian");
	cv::namedWindow("Blur");
	cv::namedWindow("Median");
	cv::namedWindow("Bilateral");

	//kernel size
	cv::Size ksize(15, 15);

	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		cv::imshow("Input", frame);
		
		//Gaussian blur
		cv::GaussianBlur(frame, gaussian, ksize, 0.0);
		cv::imshow("Gaussian", gaussian);

		//Image average blur
		cv::blur(frame, blur, ksize);
		cv::imshow("Blur", blur);

		//Median blur
		cv::medianBlur(frame, median, ksize.height);
		cv::imshow("Median", median);

		//Bilateral filter
		cv::bilateralFilter(frame, bilateral, ksize.height, 150, 150);
		cv::imshow("Bilateral", bilateral);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}