// ACGTut1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

constexpr auto WINDOW_NAME = "Display Window";

int main(int argc, char** argv)
{
    cv::VideoCapture capture(0);
    cv::Mat image;

    while (true) {
        capture.read(image);
        cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
        cv::Canny(image, image, 25, 75);
        cv::GaussianBlur(image, image, cv::Size(9, 9), 0.0, 0.0);
        cv::imshow(WINDOW_NAME, image);

        cv::waitKey(1);
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
