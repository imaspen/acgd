#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>
#include <vector>

void sum_rgb(const cv::Mat &src, cv::Mat &dst) {
	//allocate three individual image planes
	std::vector<cv::Mat> planes(3);
	
	//Split image onto the colour planes
	cv::split(src, planes);

	//Temporal storage
	cv::Mat temp(src.size(), CV_8U);

	//Add equally weighted rgb values
	cv::addWeighted(planes[0], 1. / 3., planes[2], 1. / 3., 0.0, temp);
	cv::addWeighted(temp, 1. / 3., planes[1], 1. / 3., 0.0, temp);
	cv::imshow("Temp", temp);

	//Truncate value above 100
	cv::threshold(temp, dst, 100, 255, CV_THRESH_BINARY);
}

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat threshold;

	cv::namedWindow("Original");
	cv::namedWindow("Threshold");

	while (!stop)
	{
		if (!webcam.read(frame))
			break;

		cv::imshow("Original", frame);

		sum_rgb(frame, threshold);

		cv::imshow("Threshold", threshold);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}