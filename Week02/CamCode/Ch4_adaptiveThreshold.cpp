#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	const double threshold = 100.0;
	const int threshould_type = CV_THRESH_BINARY;
	const int adaptive_method = CV_ADAPTIVE_THRESH_GAUSSIAN_C;
	const int block_size = 171;
	const int offset = 15;
	
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat gray;
	cv::Mat It;
	cv::Mat Iat;
	
	cv::namedWindow("Original");
	cv::namedWindow("Threshold");
	cv::namedWindow("Adaptive Threshold");

	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		
		//Use grayscale image for thresholding operation
		cv::cvtColor(frame, gray, CV_RGB2GRAY);

		//Threshold VS adaptive threshold
		cv::threshold(gray, It, threshold, 255, threshould_type);
		cv::adaptiveThreshold(gray, Iat, 255, adaptive_method, threshould_type, block_size, offset);
		
		cv::imshow("Original", frame);
		cv::imshow("Threshold", It);
		cv::imshow("Adaptive Threshold", Iat);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}