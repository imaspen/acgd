#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	//cv::Mat floodFill;

	cv::namedWindow("Input");
	cv::namedWindow("Flood Fill");

	//set seed point at (x=320, y=240)
	cv::Point seed(320, 240);
	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		cv::imshow("Input", frame);
		cv::floodFill(frame, seed, cv::Scalar(0, 255, 255),(cv::Rect*)0, cv::Scalar(3,3,3), cv::Scalar(3, 3, 3));

		cv::imshow("Flood Fill", frame);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}