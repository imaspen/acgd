#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>
#include <vector>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;

	//Three-layers pyramid
	const int layer = 2; //layer 0 is the original image
	std::vector<cv::Mat> pyr(layer+1);

	cv::namedWindow("Layer 1");
	cv::namedWindow("Layer 2");
	cv::namedWindow("Layer 3");


	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		pyr[0] = frame;
		for (int i = 0; i != layer; i++)
			cv::pyrDown(pyr[i], pyr[i + 1]);

		cv::imshow("Layer 1", pyr[0]);
		cv::imshow("Layer 2", pyr[1]);
		cv::imshow("Layer 3", pyr[2]);

		
		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}