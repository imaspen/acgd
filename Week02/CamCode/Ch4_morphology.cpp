#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat erode;
	cv::Mat dilate;
	cv::Mat addtional_morphology;
	cv::Mat bilateral;
	cv::namedWindow("Input");
	cv::namedWindow("Erode");
	cv::namedWindow("Dilate");
	cv::namedWindow("More General Morphology");

	//kernel for the morphology operation
	cv::Mat kernel;
	kernel=cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 9));
	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		cv::imshow("Input", frame);

		//erode operation
		cv::erode(frame, erode, kernel);
		cv::imshow("Erode", erode);

		//dilate operation
		cv::dilate(frame, dilate, kernel);
		cv::imshow("Dilate", dilate);

		//More General Morphology
		//MORPH_OPEN
		//MORPH_CLOSE 
		//MORPH_GRADIENT
		//MORPH_TOPHAT
		//MORPH_BLACKHAT
		cv::morphologyEx(frame, addtional_morphology,cv::MORPH_GRADIENT,kernel);
		cv::imshow("More General Morphology", addtional_morphology);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}