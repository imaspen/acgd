
/*
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
//*/

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"


#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	cv::Mat frame;
	cv::Mat gray;
	cv::Mat CannyImg;
	bool stop = false;
	int delay = 30; //ms
	int l_threshold = 100;
	int h_threshold = 200;
	
	while (!stop)
	{
		if (!webcam.read(frame))
			break;
		cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY); //CV_RGB2GRAY; cv::COLOR_BGR2GRAY
		cv::imshow("Input", gray);

		//Canny Edge
		cv::Canny(gray, CannyImg, l_threshold, h_threshold);
		cv::imshow("Canny", CannyImg);

		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}