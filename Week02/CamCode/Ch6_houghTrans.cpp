#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

/// Global variables

/** General variables */
cv::Mat src, edges;
cv::Mat src_gray;
cv::Mat hough;
cv::Mat indicator;
int min_threshold = 50;
int max_trackbar = 150;

const char* name = "Standard Hough Lines Demo";

int s_trackbar = max_trackbar;

void Hough(int, void*)
{
	std::vector<cv::Vec2f> s_lines;
	cvtColor(edges, hough, cv::COLOR_GRAY2BGR);

	/// 1. Use Standard Hough Transform
	HoughLines(edges, s_lines, 1, CV_PI / 180, min_threshold + s_trackbar, 0, 0);

	/// Show the result
	for (size_t i = 0; i < s_lines.size(); i++)
	{
		float r = s_lines[i][0], t = s_lines[i][1];
		double cos_t = cos(t), sin_t = sin(t);
		double x0 = r*cos_t, y0 = r*sin_t;
		double alpha = 1000;

		cv::Point pt1(cvRound(x0 + alpha*(-sin_t)), cvRound(y0 + alpha*cos_t));
		cv::Point pt2(cvRound(x0 - alpha*(-sin_t)), cvRound(y0 - alpha*cos_t));
		cv::line(hough, pt1, pt2, cv::Scalar(255, 0, 0), 3, cv::LINE_AA);
	}

	
}


int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	bool stop = false;
	int delay = 30; //ms
	cv::namedWindow(name, cv::WINDOW_AUTOSIZE);

	// Create Trackbars for Threshold 
	cv::createTrackbar("Threshold", name, &s_trackbar, max_trackbar, Hough);

	while (!stop)
	{
		if (!webcam.read(src))
			break;
		//Pass the image to gray
		cv::cvtColor(src, src_gray, CV_RGB2GRAY);

		//Apply Canny edge
		cv::Canny(src_gray, edges, 50, 200,3);

		//Renew Image
		Hough(s_trackbar,0);

		cv::addWeighted(src, 0.5, hough, 0.5, 0, indicator);

		cv::imshow(name, indicator);
		if (cv::waitKey(delay) >= 0)
			stop = true;
	}
	webcam.release();
	return 0;
}