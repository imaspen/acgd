#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\opencv.hpp>
#include <iostream>

int main()
{
	cv::VideoCapture webcam(0);

	if (!webcam.isOpened())
	{
		std::cout << "Cannot open the video cam" << std::endl;
		return -1;
	}

	int const delay = 30;
	bool stop = false;
	cv::Mat frame;
	cv::Mat segmentation;


	cv::namedWindow("Original");
	cv::namedWindow("Segmentation");



	while (!stop)
	{
		if (!webcam.read(frame))
			break;

		cv::resize(frame, frame, cv::Size(0.5*frame.cols, 0.5*frame.rows));
		cv::imshow("Original", frame);
		
		cv::pyrMeanShiftFiltering(frame, segmentation, 30, 30);
		cv::imshow("Segmentation", segmentation);

		if (cv::waitKey(delay) >= 0)
		stop = true;
	}
	webcam.release();
	return 0;
}