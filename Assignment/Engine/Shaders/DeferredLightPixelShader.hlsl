Texture2D colorTexture : register(t0);
Texture2D positionTexture : register(t1);
Texture2D normalGlossTexture : register(t2);
Texture2D lightDepthMap : register(t3);

SamplerState samplerStatePoint : register(s0);

cbuffer CameraBuffer : register(b0)
{
    float3 cameraPosition;
    float shaderMode;
}

cbuffer LightBuffer : register(b1)
{
    float3 lightPosition;
    float specularHardness;
    float3 ambientColor;
    float lightCount;
    float3 diffuseColor;
    float diffusePower;
    float3 specularColor;
    float specularPower;
}

cbuffer LightMatrixBuffer : register(b2)
{
    matrix lightViewMatrix;
    matrix lightProjectionMatrix;
}

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

float3 CalcLitColor(float3 position, float3 normal, float gloss)
{
    float3 diffuse = float3(0.0f, 0.0f, 0.0f);

    float3 lightDirection = lightPosition - position;
    float lightDistance = length(lightDirection);
    lightDirection = lightDirection / lightDistance;
    lightDistance = lightDistance * lightDistance;

#if _DEBUG
    if (shaderMode != 7.0f)
    {
#endif
        float diffuseIntensity = saturate(dot(normal, lightPosition));
        if (diffuseIntensity <= 0.0f) return float3(0.0f, 0.0f, 0.0f);

        float diffuseFalloff = 1 - saturate(lightDistance / diffusePower);
        diffuse = diffuseColor / lightCount * diffuseIntensity * diffuseFalloff;
#if _DEBUG
    }
#endif

    float3 specular = float3(0.0f, 0.0f, 0.0f);

#if _DEBUG
    if (shaderMode != 6.0f)
    {
#endif
        float lightAngle = saturate(dot(normal, lightDirection));
        if (lightAngle > 0.0f)
        {
            float3 halfVector = normalize(lightDirection + cameraPosition);
            float halfAngle = saturate(dot(normal, halfVector));
            float specularIntensity = pow(halfAngle, specularHardness);
            float specularFalloff = 1 - saturate(lightDistance / specularPower);

            specular = specularColor * specularIntensity * gloss * specularFalloff;
        }
#if _DEBUG
    }
#endif

    return diffuse + specular;
}

float4 main(PixelInputType input) : SV_TARGET
{
    float4 position = positionTexture.Sample(samplerStatePoint, input.tex);
    float4 color = colorTexture.Sample(samplerStatePoint, input.tex);
    float4 normalGloss = normalGlossTexture.Sample(samplerStatePoint, input.tex);
    float3 normal = normalGloss.xyz;
    float gloss = normalGloss.w;
    float3 outputColor = float3(0.0f, 0.0f, 0.0f);

#if _DEBUG
    switch (shaderMode)
    {
    case 0.0f: break;
    case 1.0f: return position;
    case 2.0f: return color;
    case 3.0f: return float4(normal, 1.0f);
    case 4.0f: return float4(gloss, gloss, gloss, 1.0f);
    case 5.0f: return lightDepthMap.Sample(samplerStatePoint, input.tex);
    default: break;
    }
#endif

    outputColor = ambientColor / lightCount;

    float4 lightPos = mul(position, lightViewMatrix);
    lightPos = mul(lightPos, lightProjectionMatrix);
    float2 lightTexCoords = float2(
         lightPos.x / lightPos.w / 2.0f + 0.5f,
        -lightPos.y / lightPos.w / 2.0f + 0.5f
    );

    if (saturate(lightTexCoords.x) == lightTexCoords.x && saturate(lightTexCoords.y) == lightTexCoords.y)
    {
        float lightMapDepth = lightDepthMap.Sample(samplerStatePoint, lightTexCoords).r;
        float pixelDepth = lightPos.z / lightPos.w - 0.01f;
        if (pixelDepth < lightMapDepth) outputColor += CalcLitColor(position.xyz, normal, gloss);
    }
    else outputColor += CalcLitColor(position.xyz, normal, gloss);

    return float4(saturate(color.xyz) * outputColor, 1.0f);
}
