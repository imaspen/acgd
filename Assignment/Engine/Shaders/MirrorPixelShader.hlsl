Texture2D mirrorTexture : register(t0);
Texture2D reflectionTexture : register(t1);

SamplerState samplerState : register(s0);

struct PixelInputType
{
    float4 position : SV_POSITION0;
    float2 tex : TEXCOORD0;
    float4 reflectionPosition : TEXCOORD1;
};

float4 main(PixelInputType input) : SV_TARGET
{
    float2 reflectionTexCoords = float2(
        input.reflectionPosition.x / input.reflectionPosition.w / 2.0f + 0.5f,
        -input.reflectionPosition.y / input.reflectionPosition.w / 2.0f + 0.5f
    );

    return lerp(mirrorTexture.Sample(samplerState, input.tex), reflectionTexture.Sample(samplerState, reflectionTexCoords), 0.9f);
}