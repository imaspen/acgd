cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
}

struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
    float3 instancePosition : TEXCOORD1;
    float3 instanceRotation : TEXCOORD2;
};

struct PixelInputType
{
    float4 position : SV_POSITION0;
    float4 worldPosition : TEXCOORD1;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
};

PixelInputType main(VertexInputType input)
{
    PixelInputType output;

    float4x4 translation = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        input.instancePosition.x, input.instancePosition.y, input.instancePosition.z, 1.0f
    };

    float rx = input.instanceRotation.x;
    float4x4 xRotation = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, cos(rx), -sin(rx), 0.0f,
        0.0f, sin(rx), cos(rx), 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };

    float ry = input.instanceRotation.y;
    float4x4 yRotation = {
        cos(ry), 0.0f, sin(ry), 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        -sin(ry), 0.0f, cos(ry), 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };

    float rz = input.instanceRotation.z;
    float4x4 zRotation = {
        cos(rz), -sin(rz), 0.0f, 0.0f,
        sin(rz), cos(rz), 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };

    float4x4 rotation = mul(zRotation, mul(yRotation, xRotation));

    float4x4 instanceTransformation = mul(rotation, translation);

    output.position = mul(input.position, instanceTransformation);
    output.position = mul(output.position, worldMatrix);
    output.worldPosition = output.position;
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

    output.tex = input.tex;

    output.normal = mul(input.normal, (float3x3)instanceTransformation);
    output.normal = mul(output.normal, (float3x3)worldMatrix);
    output.normal = normalize(output.normal);

    output.tangent = mul(input.tangent, (float3x3)instanceTransformation);
    output.tangent = mul(output.tangent, (float3x3)worldMatrix);
    output.tangent = normalize(output.tangent);

    output.binormal = mul(input.binormal, (float3x3)instanceTransformation);
    output.binormal = mul(output.binormal, (float3x3)worldMatrix);
    output.binormal = normalize(output.binormal);

    return output;
}
