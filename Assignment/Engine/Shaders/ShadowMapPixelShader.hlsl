struct PixelInputType
{
    float4 position : SV_POSITION0;
    float4 depthPosition : TEXCOORD0;
};

float4 main(PixelInputType input) : SV_TARGET
{
    float depth = input.depthPosition.z / input.depthPosition.w;
    return float4(depth, depth, depth, 1.0f);
}