Texture2D albedoTexture : register(t0);
Texture2D normalMap : register(t1);
Texture2D glossMap : register(t2);

SamplerState samplerState : register(s0);

struct PixelInputType
{
    float4 position : SV_POSITION0;
    float4 worldPosition : TEXCOORD1;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
};

struct PixelOutputType
{
    float4 color : SV_Target0;
    float4 position : SV_Target1;
    float4 normalGloss : SV_Target2;
};

PixelOutputType main(PixelInputType input)
{
    PixelOutputType output;

    float4 bumpMap = normalMap.Sample(samplerState, input.tex);
    bumpMap = bumpMap * 2.0f - 1.0f;
    float3 bumpNormal = input.normal + bumpMap.x * input.tangent + bumpMap.y * input.binormal;
    bumpNormal = normalize(bumpNormal);
    float4 gloss = glossMap.Sample(samplerState, input.tex);

    output.position = input.worldPosition;
    output.color = albedoTexture.Sample(samplerState, input.tex);
    output.normalGloss = float4(bumpNormal, gloss.w);

    return output;
}