Texture2D albedoTexture : register(t0);

SamplerState samplerState : register(s0);

struct PixelInputType
{
    float4 position : SV_POSITION0;
    float4 worldPosition : TEXCOORD1;
    float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

struct PixelOutputType
{
    float4 color : SV_Target0;
    float4 position : SV_Target1;
    float4 normalGloss : SV_Target2;
};

PixelOutputType main(PixelInputType input)
{
    PixelOutputType output;

    output.position = input.worldPosition;
    output.color = albedoTexture.Sample(samplerState, input.tex);
    output.normalGloss = float4(input.normal, 1);

	return output;
}