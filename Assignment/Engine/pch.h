#pragma once

// std library
#include <algorithm>
#include <exception>
#include <fstream>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

// DirectX
#include <d3d11.h>
#include <DirectXMath.h>

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>

// Others
#include <wrl/client.h>

#include "Source/DDSTextureLoader.h"

namespace DX
{
    inline void ThrowIfFailed(const HRESULT hr)
    {
        if (hr < 0)
        {
            throw std::exception();
        }
    }

    inline std::wstring StringToWString(const std::string& string)
    {
        return std::wstring(string.begin(), string.end());
    }
}