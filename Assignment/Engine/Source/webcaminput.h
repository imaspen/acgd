#pragma once

#include "controllerinterface.h"

class WebcamInput
{
public: 
	WebcamInput(int defaultSource, std::shared_ptr<ControllerInterface> ci);

	void Process();
	void Frame();

	static void MouseHandler(int event, int x, int y, int flags, void* param);
private:
	std::shared_ptr<ControllerInterface> m_ci;

	std::vector<cv::Point2f> m_points0, m_points1;
	std::vector<cv::Scalar> m_colors;

	std::unique_ptr<cv::VideoCapture> m_cap;

	std::shared_ptr<cv::Mat> m_lastGray, m_mask;
	std::shared_ptr<cv::Point2f> m_homePoint;
};
