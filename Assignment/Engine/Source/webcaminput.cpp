#include "pch.h"
#include "webcaminput.h"
#include "systemclass.h"

#include <iostream>

const std::string WINDOW_NAME = "Preview Window";
const std::string NO_HEAD_TRACKING = "Test Text";

WebcamInput::WebcamInput(int defaultSource, std::shared_ptr<ControllerInterface> ci) :
	m_ci(std::move(ci))
{
	m_cap = std::make_unique<cv::VideoCapture>(defaultSource);
	m_mask = std::make_shared<cv::Mat>();
	m_lastGray = std::make_shared<cv::Mat>();
	m_homePoint = std::make_shared<cv::Point2f>();
}

typedef std::tuple<
	std::shared_ptr<cv::Mat>,
	std::shared_ptr<cv::Point2f>,
	std::vector<cv::Point2f>*
> MouseHandlerTuple;
void WebcamInput::MouseHandler(int event, int x, int y, int flags, void* param) {
	switch (event) {
	case cv::EVENT_LBUTTONDOWN:
	{
		const auto &[ gray, homePoint, points ] = *static_cast<MouseHandlerTuple*>(param);
		*gray = cv::Mat::zeros(gray->size(), gray->type());
		points->clear();
		points->emplace_back(x, y);
		*homePoint = cv::Point2f(x, y);
		break;
	}
	default:
		break;
	}
}

void WebcamInput::Process() {
	cv::namedWindow(WINDOW_NAME, cv::WINDOW_GUI_EXPANDED | cv::WINDOW_AUTOSIZE);
	cv::setWindowProperty(WINDOW_NAME, cv::WND_PROP_TOPMOST, true);

	auto tuple = std::make_tuple(m_mask, m_homePoint, &m_points0);

	cv::setMouseCallback(WINDOW_NAME, &WebcamInput::MouseHandler, &tuple);

	cv::RNG rng;
	m_colors.push_back(cv::Scalar(255, 209, 56));
	for (int i = 0; i < 100; i++)
	{
		const int r = rng.uniform(0, 256);
		const int g = rng.uniform(0, 256);
		const int b = rng.uniform(0, 256);
		m_colors.push_back(cv::Scalar(b, g, r));
	}

	// Take first frame and use it to set up the mask
	cv::Mat frame;

	m_cap->read(frame);
	cv::flip(frame, frame, 1);
	cv::cvtColor(frame, *m_lastGray, cv::COLOR_BGR2GRAY);

	*m_mask = cv::Mat::zeros(frame.size(), frame.type());

	while (!SystemClass::m_done) {
		this->Frame();
		cv::waitKey(1);
	}
}

void WebcamInput::Frame() {
    cv::Mat frame, frame_gray;
	cv::Mat empty = cv::Mat::zeros(m_mask->size(), m_mask->type());

    m_cap->read(frame);
	cv::flip(frame, frame, 1);
	cv::cvtColor(frame, frame_gray, cv::COLOR_BGR2GRAY);

	cv::Mat overlay;
	frame.copyTo(overlay);
	cv::rectangle(overlay, cv::Rect2i(0, 0, frame.size().width, 32), cv::Scalar(0.0, 0.0, 0.0, 0.5), -1);
	constexpr auto alpha = 0.5;
	cv::addWeighted(overlay, alpha, frame, 1 - alpha, 0, frame);

	std::vector<uchar> status;
	std::vector<float> err;
	std::vector<cv::Point2f> good_new;

	if (m_points0.size() > 0) {
		const cv::TermCriteria criteria((cv::TermCriteria::COUNT)+(cv::TermCriteria::EPS), 10, 0.03);
		cv::calcOpticalFlowPyrLK(*m_lastGray, frame_gray, m_points0, m_points1, status, err, cv::Size(15, 15), 2, criteria);

		for (uint i = 0; i < m_points0.size(); i++)
		{
			// Select good points
			if (status.at(i) == 1) {
				good_new.push_back(m_points1.at(i));
				// draw the tracks
				constexpr auto alpha = 0.9;
				cv::addWeighted(*m_mask, alpha, empty, 1 - alpha, 0, *m_mask);
				line(*m_mask, m_points1.at(i), m_points0.at(i), m_colors.at(i), 2);
				circle(frame, m_points1.at(i), 5, m_colors.at(i), -1);
			}
		}
	    m_points0 = good_new;

		// calculate offset
		if (m_points0.size() > 0) {
			const auto &currPoint = m_points0.at(0);

			if (m_ci->m_resetHomeFlag.load()) {
				m_ci->m_resetHomeFlag = false;
				*m_homePoint = currPoint;
			}

			const auto offset = currPoint - *m_homePoint;
			const auto xDeadzoneSize = frame.size().width * 0.1;
			const auto yDeadzoneSize = frame.size().height * 0.1;

			//const float xMovement = std::abs(offset.x) < xDeadzoneSize ? 0 : (offset.x > 0 ? 1 : -1);
			//const float yMovement = std::abs(offset.y) < yDeadzoneSize ? 0 : (offset.y > 0 ? 1 : -1);

			const float xMovement = offset.x / (frame.size().width / 2);
			const float yMovement = offset.y / (frame.size().height / 2);

			const std::string xLabel = xMovement > 0 ? "Right" : (xMovement < 0 ? "Left" : "None");
			const std::string yLabel = yMovement > 0 ? "Down" : (yMovement < 0 ? "Up" : "None");

			cv::putText(frame, yLabel + " " + xLabel, cv::Point2i(8, 16), cv::FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);

			const auto rotation = DirectX::XMLoadFloat2(&DirectX::XMFLOAT2(yMovement, xMovement));

			m_ci->setRotation(rotation);
		}
	} else {
		*m_mask = cv::Mat::zeros(frame.size(), frame.type());
		cv::putText(frame, "Face tracking disabled. Click a point to track to enable.", cv::Point2i(8, 16), cv::FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
	}

    // Now update the previous frame and previous points
    *m_lastGray = frame_gray.clone();

	cv::add(frame, *m_mask, frame);
	cv::imshow(WINDOW_NAME, frame);
}
