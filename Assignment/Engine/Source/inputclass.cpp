#include "pch.h"
#include "inputclass.h"

#include <iterator>

using namespace DirectX;

InputClass::InputClass(HINSTANCE hinstance, HWND hwnd)
{
    // Initialize the main direct input interface.
    DX::ThrowIfFailed(DirectInput8Create(
        hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8,
        reinterpret_cast<void**>(&m_directInput), nullptr
    ));

    // Initialize the direct input interface for the keyboard.
    DX::ThrowIfFailed(m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, nullptr));

    // Set the data format.  In this case since it is a keyboard we can use the predefined data format.
    DX::ThrowIfFailed(m_keyboard->SetDataFormat(&c_dfDIKeyboard));

    // Set the cooperative level of the keyboard to not share with other programs.
    DX::ThrowIfFailed(m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));

    // Now acquire the keyboard.
    DX::ThrowIfFailed(m_keyboard->Acquire());
}


InputClass::~InputClass()
{
    // Release the keyboard.
    if (m_keyboard)
    {
        m_keyboard->Unacquire();
        m_keyboard->Release();
    }

    // Release the main interface to direct input.
    if (m_directInput)
    {
        m_directInput->Release();
    }
}


bool InputClass::Frame()
{
    // Read the current state of the keyboard.
    auto result = ReadKeyboard();
    if (!result) return false;

    // Process the changes in the mouse and keyboard.
    ProcessInput();

    return true;
}


bool InputClass::ReadKeyboard()
{
    // Read the keyboard device.
    std::copy(std::begin(m_keyboardState), std::end(m_keyboardState), std::begin(m_keyboardLastState));

    const auto result = m_keyboard->GetDeviceState(sizeof m_keyboardState, static_cast<LPVOID>(&m_keyboardState));
    if (FAILED(result))
    {
        // If the keyboard lost focus or was not acquired then try to get control back.
        if (result == DIERR_INPUTLOST || result == DIERR_NOTACQUIRED)
        {
            m_keyboard->Acquire();
        }
        else
        {
            return false;
        }
    }

    return true;
}


void InputClass::ProcessInput()
{
}

// Returns the pressed state of any key
bool InputClass::IsPressed(const unsigned char& key) const noexcept
{
    return m_keyboardState[key] & 0x80;
}

// Returns true if the key was released this frame
bool InputClass::KeyReleased(const unsigned char& key) const noexcept
{
    return !(m_keyboardState[key] & 0x80) && m_keyboardLastState[key] & 0x80;
}
