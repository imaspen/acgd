#include "pch.h"
#include "systemclass.h"

#include <thread>

std::atomic_bool SystemClass::m_done = false;

SystemClass::SystemClass()
{
	InitializeWindows();
	m_ci = std::make_shared<ControllerInterface>();
	m_webcamInput = std::make_shared<WebcamInput>(0, m_ci);
	m_input = std::make_shared<InputClass>(m_hinstance, m_hwnd);
	m_game = std::make_unique<AspenRenderer::Game>(m_input, m_ci, m_hwnd, m_screenWidth, m_screenHeight);
}

SystemClass::~SystemClass()
{
	// Shutdown the window.
	ShutdownWindows();
}

void SystemClass::Run() const
{
	std::thread gameThread([this] { this->Process(); });
	std::thread webcamThread(&WebcamInput::Process, m_webcamInput);

	MSG msg;

    // Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user.
	while (!SystemClass::m_done)
	{
		// Handle the windows messages.
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if (msg.message == WM_QUIT)
		{
			SystemClass::m_done = true;
		}
	}

	gameThread.join();
	webcamThread.join();
}

void SystemClass::Process() const
{
	while (!SystemClass::m_done) {
		const auto rotation = m_ci->getRotation();

		// Read the user input.
		if (!m_input->Frame())
		{
			SystemClass::m_done = true;
		}

		// Check if the user pressed escape and wants to exit the application.
		if (m_input->IsEscapePressed() == true)
		{
			SystemClass::m_done = true;
		}

		// Do the frame processing for the graphics object.
		m_game->Frame();
	}
}

LRESULT CALLBACK SystemClass::MessageHandler(HWND hwnd, const UINT umsg, const WPARAM wparam, const LPARAM lparam) noexcept
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

void SystemClass::InitializeWindows() noexcept
{
    // Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	m_hinstance = GetModuleHandle(nullptr);

	// Give the application a name.
	m_applicationName = L"Engine";

	// Setup the windows class with default settings.
	WNDCLASSEX wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hinstance;
	wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	wc.lpszMenuName = nullptr;
	wc.lpszClassName = m_applicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	m_screenWidth = GetSystemMetrics(SM_CXSCREEN);
	m_screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	auto posX = 0, posY = 0;
	if (FULL_SCREEN)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		DEVMODE dmScreenSettings;
		memset(&dmScreenSettings, 0, sizeof dmScreenSettings);
		dmScreenSettings.dmSize = sizeof dmScreenSettings;
		dmScreenSettings.dmPelsWidth = m_screenWidth;
		dmScreenSettings.dmPelsHeight = m_screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
	}
	else
	{
		// If windowed then set it to screen resolution, max 1920x1080.
		m_screenWidth = GetSystemMetrics(SM_CXSCREEN);
		m_screenHeight = GetSystemMetrics(SM_CYSCREEN);

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - m_screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - m_screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX, posY, m_screenWidth, m_screenHeight, nullptr, nullptr, m_hinstance, nullptr);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);
}

void SystemClass::ShutdownWindows() noexcept
{
	// Fix the display settings if leaving full screen mode.
	if (FULL_SCREEN)
	{
		ChangeDisplaySettings(nullptr, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = nullptr;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = nullptr;

	// Release the pointer to this class.
	ApplicationHandle = nullptr;
}

LRESULT CALLBACK WndProc(HWND hwnd, const UINT umessage, const WPARAM wparam, const LPARAM lparam) noexcept
{
	switch (umessage)
	{
	// Check if the window is being destroyed.
	case WM_DESTROY:
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;
	}
	// All other messages pass to the message handler in the system class.
	default:
	{
		return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
	}
	}
}
