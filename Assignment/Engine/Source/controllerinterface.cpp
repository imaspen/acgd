#include "pch.h"
#include "controllerinterface.h"

ControllerInterface::ControllerInterface()
{
	m_rotation = std::make_unique<DirectX::XMFLOAT2>(0.0f, 0.0f);
}

DirectX::XMVECTOR ControllerInterface::getRotation() const
{
	std::lock_guard lock(m_rotationLock);
	return DirectX::XMLoadFloat2(m_rotation.get());
}

void ControllerInterface::setRotation(const DirectX::XMVECTOR newRotation)
{
	std::lock_guard lock(m_rotationLock);
	DirectX::XMStoreFloat2(m_rotation.get(), newRotation);
}
