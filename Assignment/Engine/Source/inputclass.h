#pragma once

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#define DIRECTINPUT_VERSION 0x0800

#include <dinput.h>

class InputClass
{
public:
    InputClass(HINSTANCE hinstance, HWND hwnd);
    InputClass(const InputClass&) = delete;
    InputClass(InputClass&&) = delete;
    ~InputClass();
    InputClass& operator= (const InputClass&) = delete;
    InputClass& operator= (InputClass&&) = delete;

    bool Frame();

    bool IsEscapePressed() const noexcept { return IsPressed(DIK_ESCAPE); }
    bool IsLeftPressed() const noexcept { return IsPressed(DIK_LEFT); }
    bool IsRightPressed() const noexcept { return IsPressed(DIK_RIGHT); }
    bool IsUpPressed() const noexcept { return IsPressed(DIK_UP); }
    bool IsDownPressed() const noexcept { return IsPressed(DIK_DOWN); }
    bool IsAPressed() const noexcept { return IsPressed(DIK_A); }
    bool IsZPressed() const noexcept { return IsPressed(DIK_Z); }
    bool IsPgUpPressed() const noexcept { return IsPressed(DIK_PGUP); }
    bool IsPgDownPressed() const noexcept { return IsPressed(DIK_PGDN); }

    bool IsWPressed() const noexcept { return IsPressed(DIK_W); }
    bool IsSPressed() const noexcept { return IsPressed(DIK_S); }
    bool IsDPressed() const noexcept { return IsPressed(DIK_D); }

    bool IsEReleased() const noexcept { return KeyReleased(DIK_E); }
    bool IsRReleased() const noexcept { return KeyReleased(DIK_R); }

    bool IsShiftPressed() const noexcept { return IsPressed(DIK_LSHIFT); }

    bool Is0Released() const noexcept { return KeyReleased(DIK_0); }
    bool Is1Released() const noexcept { return KeyReleased(DIK_1); }
    bool Is2Released() const noexcept { return KeyReleased(DIK_2); }
    bool Is3Released() const noexcept { return KeyReleased(DIK_3); }
    bool Is4Released() const noexcept { return KeyReleased(DIK_4); }
    bool Is5Released() const noexcept { return KeyReleased(DIK_5); }
    bool Is6Released() const noexcept { return KeyReleased(DIK_6); }
    bool Is7Released() const noexcept { return KeyReleased(DIK_7); }
    bool Is8Released() const noexcept { return KeyReleased(DIK_8); }
    bool Is9Released() const noexcept { return KeyReleased(DIK_9); }

private:
    void ProcessInput();
    bool ReadKeyboard();

    // My private functions
    bool IsPressed(const unsigned char & key) const noexcept;
    bool KeyReleased(const unsigned char & key) const noexcept;

    IDirectInput8* m_directInput{};
    IDirectInputDevice8* m_keyboard{};

    unsigned char m_keyboardState[256]{};
    unsigned char m_keyboardLastState[256]{};
};
