#pragma once

#include <Windows.h>

class TimerClass
{
public:
	TimerClass() noexcept;

	void Frame() noexcept;

	float GetTime() const noexcept;

private:
	LARGE_INTEGER m_frequency{};
	float m_ticksPerMs = 0.0f;
	LARGE_INTEGER m_startTime{};
	float m_frameTime = 0.0f;
};
