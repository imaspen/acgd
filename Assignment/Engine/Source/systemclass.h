#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>

#include "controllerinterface.h"
#include "inputclass.h"
#include "webcaminput.h"
#include "AspenRenderer/Game.h"

class SystemClass
{
public:
    SystemClass();
    SystemClass(const SystemClass&) = delete;
    SystemClass(SystemClass&&) = delete;
    ~SystemClass();
    SystemClass& operator= (const SystemClass&) = delete;
    SystemClass& operator= (SystemClass&&) = delete;

    void Run() const;
    void Process() const;

    static LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM) noexcept;

    static std::atomic_bool m_done;

private:
    void InitializeWindows() noexcept;
    void ShutdownWindows() noexcept;

    int m_screenWidth = 0, m_screenHeight = 0;

    LPCWSTR m_applicationName{};
    HINSTANCE m_hinstance{};
    HWND m_hwnd{};

    std::shared_ptr<ControllerInterface> m_ci;
    std::shared_ptr<WebcamInput> m_webcamInput;
    std::shared_ptr<InputClass> m_input;
    std::unique_ptr<AspenRenderer::Game> m_game;
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM) noexcept;
static SystemClass* ApplicationHandle = nullptr;
