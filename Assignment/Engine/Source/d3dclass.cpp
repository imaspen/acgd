#include "pch.h"
#include "d3dclass.h"

#include <iostream>

using namespace DirectX;

D3DClass::D3DClass(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, float screenDepth, float screenNear) :
    m_vsyncEnabled(vsync)
{
    // Create a DirectX graphics interface factory.
    IDXGIFactory* factory = nullptr;
    DX::ThrowIfFailed(CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&factory)));

    // Use the factory to create an adapter for the primary graphics interface (video card).
    IDXGIAdapter* adapter = nullptr;
    DX::ThrowIfFailed(factory->EnumAdapters(0, &adapter));

    // Enumerate the primary adapter output (monitor).
    IDXGIOutput* adapterOutput = nullptr;
    DX::ThrowIfFailed(adapter->EnumOutputs(0, &adapterOutput));

    // Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
    unsigned int numModes = 0;
    DX::ThrowIfFailed(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, nullptr));

    // Create a list to hold all the possible display modes for this monitor/video card combination.
    auto* displayModeList = new DXGI_MODE_DESC[numModes];
    if (!displayModeList) throw std::exception();

    // Now fill the display mode list structures.
    DX::ThrowIfFailed(adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList));

    // Now go through all the display modes and find the one that matches the screen width and height.
    // When a match is found store the numerator and denominator of the refresh rate for that monitor.
    unsigned int numerator = 0, denominator = 0;
    for (unsigned int i = 0; i < numModes; i++)
    {
        if (displayModeList[i].Width == static_cast<unsigned int>(screenWidth))
        {
            if (displayModeList[i].Height == static_cast<unsigned int>(screenHeight))
            {
                numerator = displayModeList[i].RefreshRate.Numerator;
                denominator = displayModeList[i].RefreshRate.Denominator;
            }
        }
    }

    // Get the adapter (video card) description.
    DXGI_ADAPTER_DESC adapterDesc;
    DX::ThrowIfFailed(adapter->GetDesc(&adapterDesc));

    // Store the dedicated video card memory in megabytes.
    m_videoCardMemory = static_cast<int>(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

    // Convert the name of the video card to a character array and store it.
    size_t stringLength = 0;
    const auto error = wcstombs_s(&stringLength, m_videoCardDescription, 128, adapterDesc.Description, 128);
    if (error != 0) throw std::exception();

    // Release the display mode list.
    delete[] displayModeList;

    // Release the adapter output.
    adapterOutput->Release();

    // Release the adapter.
    adapter->Release();

    // Release the factory.
    factory->Release();

    // Initialize the swap chain description.
    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    ZeroMemory(&swapChainDesc, sizeof swapChainDesc);

    // Set to a single back buffer.
    swapChainDesc.BufferCount = 2;

    // Set the width and height of the back buffer.
    swapChainDesc.BufferDesc.Width = screenWidth;
    swapChainDesc.BufferDesc.Height = screenHeight;

    // Set regular 32-bit surface for the back buffer.
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

    // Set the refresh rate of the back buffer.
    if (m_vsyncEnabled)
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
    }
    else
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    }

    // Set the usage of the back buffer.
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

    // Set the handle for the window to render to.
    swapChainDesc.OutputWindow = hwnd;

    // Turn multisampling off.
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;

    // Set to full screen or windowed mode.
    if (fullscreen)
    {
        swapChainDesc.Windowed = false;
    }
    else
    {
        swapChainDesc.Windowed = true;
    }

    swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    // Set the scan line ordering and scaling to unspecified.
    swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    // Discard the back buffer contents after presenting.
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

    // Don't set the advanced flags.
    swapChainDesc.Flags = 0;

    // Set the feature level to DirectX 11.
    auto featureLevel = D3D_FEATURE_LEVEL_11_0;

/* crashed uni computers #if !_DEBUG
    const auto flags = D3D11_CREATE_DEVICE_DEBUG;
#else*/
    const auto flags = 0;
//#endif

    // Create the swap chain, Direct3D device, and Direct3D device context.
    DX::ThrowIfFailed(D3D11CreateDeviceAndSwapChain(
        nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, flags, &featureLevel, 1,
        D3D11_SDK_VERSION, &swapChainDesc, m_swapChain.ReleaseAndGetAddressOf(), 
        m_device.ReleaseAndGetAddressOf(), nullptr, m_deviceContext.ReleaseAndGetAddressOf()
    ));

    // Get the pointer to the back buffer.
    ID3D11Texture2D* backBufferPtr = nullptr;
    DX::ThrowIfFailed(m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<LPVOID*>(&backBufferPtr)));

    // Create the render target view with the back buffer pointer.
    DX::ThrowIfFailed(m_device->CreateRenderTargetView(backBufferPtr, nullptr, m_renderTargetView.ReleaseAndGetAddressOf()));

    // Release pointer to the back buffer as we no longer need it.
    backBufferPtr->Release();

    // Initialize the description of the depth buffer.
    D3D11_TEXTURE2D_DESC depthBufferDesc;
    ZeroMemory(&depthBufferDesc, sizeof depthBufferDesc);

    // Set up the description of the depth buffer.
    depthBufferDesc.Width = screenWidth;
    depthBufferDesc.Height = screenHeight;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    // Create the texture for the depth buffer using the filled out description.
    DX::ThrowIfFailed(m_device->CreateTexture2D(&depthBufferDesc, nullptr, m_depthStencilBuffer.ReleaseAndGetAddressOf()));

    // Initialize the description of the stencil state.
    D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
    ZeroMemory(&depthStencilDesc, sizeof depthStencilDesc);

    // Set up the description of the stencil state.
    depthStencilDesc.DepthEnable = true;
    depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

    depthStencilDesc.StencilEnable = true;
    depthStencilDesc.StencilReadMask = 0xFF;
    depthStencilDesc.StencilWriteMask = 0xFF;

    // Stencil operations if pixel is front-facing.
    depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Stencil operations if pixel is back-facing.
    depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
    depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Create the depth stencil state.
    DX::ThrowIfFailed(m_device->CreateDepthStencilState(&depthStencilDesc, m_depthStencilState.ReleaseAndGetAddressOf()));

    // Set the depth stencil state.
    m_deviceContext->OMSetDepthStencilState(m_depthStencilState.Get(), 1);

    D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
    ZeroMemory(&depthDisabledStencilDesc, sizeof depthDisabledStencilDesc);

    // Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
    // that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
    depthDisabledStencilDesc.DepthEnable = false;
    depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
    depthDisabledStencilDesc.StencilEnable = true;
    depthDisabledStencilDesc.StencilReadMask = 0xFF;
    depthDisabledStencilDesc.StencilWriteMask = 0xFF;
    depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
    depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
    depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Create the state using the device.
    DX::ThrowIfFailed(m_device->CreateDepthStencilState(&depthDisabledStencilDesc, m_depthDisabledStencilState.ReleaseAndGetAddressOf()));

    // Initialize the depth stencil view.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(&depthStencilViewDesc, sizeof depthStencilViewDesc);

    // Set up the depth stencil view description.
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    // Create the depth stencil view.
    DX::ThrowIfFailed(m_device->CreateDepthStencilView(m_depthStencilBuffer.Get(), &depthStencilViewDesc, m_depthStencilView.ReleaseAndGetAddressOf()));

    // Bind the render target view and depth stencil buffer to the output render pipeline.
    m_deviceContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());

    // Setup the raster description which will determine how and what polygons will be drawn.
    D3D11_RASTERIZER_DESC rasterDesc;
    rasterDesc.AntialiasedLineEnable = false;
    rasterDesc.CullMode = D3D11_CULL_BACK;
    rasterDesc.DepthBias = 0;
    rasterDesc.DepthBiasClamp = 0.0f;
    rasterDesc.DepthClipEnable = true;
    rasterDesc.FillMode = D3D11_FILL_SOLID;
    rasterDesc.FrontCounterClockwise = false;
    rasterDesc.MultisampleEnable = false;
    rasterDesc.ScissorEnable = false;
    rasterDesc.SlopeScaledDepthBias = 0.0f;

    // Create the rasterizer state from the description we just filled out.
    DX::ThrowIfFailed(m_device->CreateRasterizerState(&rasterDesc, m_rasterState.ReleaseAndGetAddressOf()));

    // Now set the rasterizer state.
    m_deviceContext->RSSetState(m_rasterState.Get());

    // Setup blend description.
    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));

    // Create the blend enabled state.
    blendDesc.RenderTarget[0].BlendEnable = TRUE;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

    DX::ThrowIfFailed(m_device->CreateBlendState(&blendDesc, m_blendEnabledState.ReleaseAndGetAddressOf()));

    // Create the blend disabled state.
    blendDesc.RenderTarget[0].BlendEnable = FALSE;

    DX::ThrowIfFailed(m_device->CreateBlendState(&blendDesc, m_blendDisabledState.ReleaseAndGetAddressOf()));

    // Create the color additive blend state.
    blendDesc.RenderTarget[0].BlendEnable = TRUE;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_RED | D3D11_COLOR_WRITE_ENABLE_GREEN | D3D11_COLOR_WRITE_ENABLE_BLUE;


    m_device->CreateBlendState(&blendDesc, m_colorAddBlendState.ReleaseAndGetAddressOf());

    // Setup the viewport for rendering.
    m_viewport.Width = static_cast<float>(screenWidth);
    m_viewport.Height = static_cast<float>(screenHeight);
    m_viewport.MinDepth = 0.0f;
    m_viewport.MaxDepth = 1.0f;
    m_viewport.TopLeftX = 0.0f;
    m_viewport.TopLeftY = 0.0f;

    // Create the viewport.
    m_deviceContext->RSSetViewports(1, &m_viewport);

    // Setup the projection matrix.
    auto screenAspect = static_cast<float>(screenWidth) / static_cast<float>(screenHeight);

    // Create the projection matrix for 3D rendering.
    XMStoreFloat4x4(&m_projectionMatrix, XMMatrixPerspectiveFovLH(XM_PIDIV2, screenAspect, screenNear, screenDepth));

    // Initialize the world matrix to the identity matrix.
    XMStoreFloat4x4(&m_worldMatrix, XMMatrixIdentity());

    // Create an orthographic projection matrix for 2D rendering.
    XMStoreFloat4x4(&m_orthoMatrix, XMMatrixOrthographicLH(static_cast<float>(screenWidth), static_cast<float>(screenHeight), screenNear, screenDepth));
}


D3DClass::~D3DClass()
{
    try
    {
        m_swapChain->SetFullscreenState(false, nullptr);
    }
    catch (const std::exception& err)
    {
        std::cerr << err.what();
    }
}


void D3DClass::BeginScene(const float red, const float green, const float blue, const float alpha) const
{
    float color[4];


    // Setup the color to clear the buffer to.
    color[0] = red;
    color[1] = green;
    color[2] = blue;
    color[3] = alpha;

    // Clear the back buffer.
    m_deviceContext->ClearRenderTargetView(m_renderTargetView.Get(), color);

    // Clear the depth buffer.
    m_deviceContext->ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}


void D3DClass::EndScene() const
{
    // Present the back buffer to the screen since rendering is complete.
    if (m_vsyncEnabled)
    {
        // Lock to screen refresh rate.
        m_swapChain->Present(1, 0);
    }
    else
    {
        // Present as fast as possible.
        m_swapChain->Present(0, 0);
    }
    m_deviceContext->ClearState();
}


void D3DClass::GetVideoCardInfo(char* cardName, int& memory) const
{
    strcpy_s(cardName, 128, m_videoCardDescription);
    memory = m_videoCardMemory;
}


void D3DClass::EnableColorBlending() const
{
    float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

    // Turn on the color blending.
    m_deviceContext->OMSetBlendState(m_colorAddBlendState.Get(), blendFactor, 0xffffffff);
}


void D3DClass::EnableAlphaBlending() const
{
    float blendFactor[4];

    // Setup the blend factor.
    blendFactor[0] = 0.0f;
    blendFactor[1] = 0.0f;
    blendFactor[2] = 0.0f;
    blendFactor[3] = 0.0f;

    // Turn on the alpha blending.
    m_deviceContext->OMSetBlendState(m_blendEnabledState.Get(), blendFactor, 0xffffffff);
}


void D3DClass::DisableAlphaBlending() const
{
    float blendFactor[4];

    // Setup the blend factor.
    blendFactor[0] = 0.0f;
    blendFactor[1] = 0.0f;
    blendFactor[2] = 0.0f;
    blendFactor[3] = 0.0f;

    // Turn off the alpha blending.
    m_deviceContext->OMSetBlendState(m_blendDisabledState.Get(), blendFactor, 0xffffffff);
}

void D3DClass::SetBackBufferRenderTarget(const Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& depthStencil)
{
    ResetViewport();
    if (depthStencil == nullptr)
    {
        // Bind the render target view and depth stencil buffer to the output render pipeline.
        m_deviceContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());
        return;
    }
    m_deviceContext->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), depthStencil.Get());
}

void D3DClass::TurnZBufferOn() const
{
    m_deviceContext->OMSetDepthStencilState(m_depthStencilState.Get(), 1);
}

void D3DClass::TurnZBufferOff() const
{
    m_deviceContext->OMSetDepthStencilState(m_depthDisabledStencilState.Get(), 1);
}

void D3DClass::ResetViewport() const
{
    m_deviceContext->RSSetViewports(1, &m_viewport);
}
