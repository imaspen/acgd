#pragma once

#include "Camera.h"
#include "Controller.h"
#include "RenderManager.h"
#include "GameObjects/GameObject.h"
#include "GameObjects/GameObjects.h"
#include "Source/d3dclass.h"
#include "Source/inputclass.h"
#include "Source/timerclass.h"
#include "../controllerinterface.h"

constexpr bool FULL_SCREEN = false;
constexpr bool VSYNC_ENABLED = false;
constexpr float SCREEN_DEPTH = 100.0f;
constexpr float SCREEN_NEAR = 0.1f;

namespace AspenRenderer
{
    class Game
    {
    public:
        Game(std::shared_ptr<InputClass>, std::shared_ptr<ControllerInterface>&, HWND, int, int);

        void Frame();

    private:
        void Tick(float deltaTime) const;
        void Render() const;

        std::shared_ptr<D3DClass> m_d3d;
        std::shared_ptr<InputClass> m_input;
        std::shared_ptr<Camera> m_camera;
        std::shared_ptr<Controller> m_controller;
        std::shared_ptr<RenderManager> m_renderManager;

        TimerClass m_timer;
        std::unique_ptr<GameObjects::GameObject> m_rootGameObject;
        std::unique_ptr<GameObjects::AlbedoGameObject> m_ballObject;
        std::list<std::shared_ptr<GameObjects::ModelGameObject>> m_physicsObjects;
        float m_renderMode = 0.0f;
        unsigned int m_isolatedLight = 0;
    };   
}
