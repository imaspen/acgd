#include "pch.h"
#include "Game.h"

#include "Models/Model.h"
#include "RenderManager.h"
#include "GameObjects/RootGameObject.h"

using namespace AspenRenderer::GameObjects;
using namespace DirectX;
using AspenRenderer::Models::Model;

AspenRenderer::Game::Game(std::shared_ptr<InputClass> input, std::shared_ptr<ControllerInterface>& ci, HWND hwnd, const int screenWidth, const int screenHeight) :
    m_input(std::move(input))
{
    m_d3d = std::make_shared<D3DClass>(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
    m_camera = std::make_shared<Camera>(XMVectorSet(0.0f, 1.7f, -0.75f, 0.0f), XMVectorSet(50.0f, 0.0f, 0.0f, 0.0f));
    m_controller = std::make_shared<Controller>(m_input, m_camera, ci);
    m_renderManager = std::make_shared<RenderManager>(m_d3d, m_camera, screenWidth, screenHeight);
    m_rootGameObject = std::make_unique<RootGameObject>(m_renderManager, m_d3d, m_camera);
    m_physicsObjects = m_rootGameObject->GetPhysicsObjects();
    m_ballObject = std::make_unique<AlbedoGameObject>(m_renderManager, std::make_shared<Model>(m_d3d->GetDevice(), "data/Sphere/Sphere.txt"), m_d3d->GetDevice(), L"data/Sphere/Sphere.dds");
    m_ballObject->SetLocalPosition(XMVectorSet(0.0f, 5.0f, 3.0f, 0.0f));
    m_ballObject->m_acceleration = XMFLOAT3(0.0f, -9.8f, 0.0f);
    m_ballObject->m_physicsType = ModelGameObject::PhysicsType::Dynamic;
}

void AspenRenderer::Game::Frame()
{
    // Update the system stats.
    m_timer.Frame();
    const auto deltaTime = m_timer.GetTime();

    // If the E key was pressed and released, throw a ball.
    if (m_input->IsEReleased())
    {
        // Create the ball, set its location to be our location, and speed to be 10 * the look vector.
        const auto newBall = std::make_shared<AlbedoGameObject>(*m_ballObject);
        newBall->SetLocalPosition(m_camera->GetPosition());
        const auto speed = 10.0f * XMVector3Transform(XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), XMMatrixRotationRollPitchYawFromVector(m_camera->GetRotation()));
        XMStoreFloat3(&newBall->m_speed, speed);

        // Add the ball to the scene and the list of physics objects.
        m_rootGameObject->AddChild(newBall);
        m_physicsObjects.push_back(newBall);
    }

    // Tick our children.
    Tick(deltaTime);

    // Render the graphics.
    Render();
}

void AspenRenderer::Game::Tick(const float deltaTime) const
{
    // Tick physics objects.
    for (const auto& object : m_physicsObjects)
        object->PhysicsTick(m_physicsObjects, deltaTime);

    // Tick the camera controller.
    m_controller->Tick(m_physicsObjects, deltaTime);

    // Tick the game objects recursively.
    m_rootGameObject->Tick(deltaTime);
}

void AspenRenderer::Game::Render() const
{
    // Queue rendering the game objects recursively.
    m_rootGameObject->Render();

    // Do the drawing.
    m_renderManager->Render(m_isolatedLight, m_renderMode);
}
