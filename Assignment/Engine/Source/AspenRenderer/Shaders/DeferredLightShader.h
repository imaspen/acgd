#pragma once

#include "Shader.h"
#include "Source/AspenRenderer/DeferredBuffers.h"
#include "Source/AspenRenderer/PointLight.h"

namespace AspenRenderer
{
    namespace Shaders
    {
        class DeferredLightShader : public TextureShader
        {
            struct MatrixBufferType
            {
                DirectX::XMFLOAT4X4 worldMatrix;
                DirectX::XMFLOAT4X4 viewMatrix;
                DirectX::XMFLOAT4X4 projectionMatrix;
            };

            struct CameraBufferType
            {
                DirectX::XMFLOAT3 cameraPosition;
                float shaderMode;
            };

        public:
            explicit DeferredLightShader(const std::shared_ptr<D3DClass>& d3d);

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix, 
                const DirectX::XMVECTOR& cameraPosition, float shaderMode, const PointLight& light, int lightCount, DeferredBuffers& deferredBuffers);

            void RenderShader(int indexCount);

        protected:
            Microsoft::WRL::ComPtr<ID3D11Buffer> m_cameraBuffer, m_lightBuffer, m_lightMatrixBuffer;

        private:
            void CompileShaders();
            void InitializeSampler();
            void InitializeBuffers();
        };
    }
}
