#pragma once

#include "../../d3dclass.h"

namespace AspenRenderer
{
    namespace Shaders
    {
        class Shader
        {
            struct MatrixBufferType
            {
                DirectX::XMFLOAT4X4 worldMatrix;
                DirectX::XMFLOAT4X4 viewMatrix;
                DirectX::XMFLOAT4X4 projectionMatrix;
            };
        public:
            Shader(std::shared_ptr<D3DClass> d3d, const std::string& vsPath, const std::string& psPath);

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix,
                const DirectX::XMMATRIX& projectionMatrix);
            void RenderShader(int vertexCount, int instanceCount) const;

        protected:
            explicit Shader(std::shared_ptr<D3DClass> d3d) noexcept;

            std::shared_ptr<D3DClass> m_d3d;

            Microsoft::WRL::ComPtr<ID3D11VertexShader> m_vertexShader;
            Microsoft::WRL::ComPtr<ID3D11PixelShader> m_pixelShader;
            Microsoft::WRL::ComPtr<ID3D11InputLayout> m_layout;
            Microsoft::WRL::ComPtr<ID3D11Buffer> m_matrixBuffer;

            static std::vector<char> LoadCompiledShader(const std::string& path);

            void LoadCompiledShaders(const std::string& vsPath, const std::string& psPath);
            void InitializeMatrixBuffer();
        };

        class DepthShader : protected Shader
        {
        public:
            explicit DepthShader(std::shared_ptr<D3DClass> d3d);

            using Shader::SetShaderParameters;
            using Shader::RenderShader;
        };

        class TextureShader : protected Shader
        {
        public:
            TextureShader(std::shared_ptr<D3DClass> d3d, const std::string& vsPath, const std::string& psPath);

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix,
                const DirectX::XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture);
            using Shader::RenderShader;

        protected:
            explicit TextureShader(const std::shared_ptr<D3DClass>& d3d) : Shader(d3d) {}

            Microsoft::WRL::ComPtr<ID3D11SamplerState> m_samplerState;

            void InitializeSampler();
        };

        class DeferredBumpMapShader : protected TextureShader
        {
        public:
            explicit DeferredBumpMapShader(const std::shared_ptr<D3DClass> & d3d) :
                TextureShader(d3d, "DeferredBumpMapVertexShader.cso", "DeferredBumpMapPixelShader.cso")
            {}

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix,
                const DirectX::XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* bumpMap);

            using TextureShader::RenderShader;
        };

        class DeferredPBRShader : protected TextureShader
        {
        public:
            explicit DeferredPBRShader(const std::shared_ptr<D3DClass>& d3d) :
                TextureShader(d3d, "DeferredPBRVertexShader.cso", "DeferredPBRPixelShader.cso")
            {}

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix,
                const DirectX::XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture,
                ID3D11ShaderResourceView* bumpMap, ID3D11ShaderResourceView* glossMap);

            using TextureShader::RenderShader;
        };
    }
}
