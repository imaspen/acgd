#include "pch.h"
#include "DeferredLightShader.h"

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::Shaders::DeferredLightShader::DeferredLightShader(const std::shared_ptr<D3DClass>& d3d) :
    TextureShader(d3d)
{
    CompileShaders();
    InitializeBuffers();
    InitializeSampler();
}

void AspenRenderer::Shaders::DeferredLightShader::SetShaderParameters(const XMMATRIX& worldMatrix,
    const XMMATRIX& viewMatrix, const XMMATRIX& projectionMatrix, const XMVECTOR& cameraPosition,
    const float shaderMode, const PointLight& light, const int lightCount, DeferredBuffers& deferredBuffers)
{
    auto& deviceContext = m_d3d->GetDeviceContext();
    D3D11_MAPPED_SUBRESOURCE mappedResource{};

    // Update the matrix cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_matrixBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* matrixBuffer = static_cast<MatrixBufferType*>(mappedResource.pData);
    if (matrixBuffer == nullptr) throw std::exception();

    XMStoreFloat4x4(&matrixBuffer->worldMatrix, XMMatrixTranspose(worldMatrix));
    XMStoreFloat4x4(&matrixBuffer->viewMatrix, XMMatrixTranspose(viewMatrix));
    XMStoreFloat4x4(&matrixBuffer->projectionMatrix, XMMatrixTranspose(projectionMatrix));

    deviceContext.Unmap(m_matrixBuffer.Get(), 0);
    deviceContext.VSSetConstantBuffers(0, 1, m_matrixBuffer.GetAddressOf());

    // Update the camera cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_cameraBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* cameraBuffer = static_cast<CameraBufferType*>(mappedResource.pData);

    XMStoreFloat3(&cameraBuffer->cameraPosition, cameraPosition);
    cameraBuffer->shaderMode = shaderMode;

    deviceContext.Unmap(m_cameraBuffer.Get(), 0);
    deviceContext.PSSetConstantBuffers(0, 1, m_cameraBuffer.GetAddressOf());

    // Update the light cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_lightBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* lightBuffer = static_cast<PointLight::LightBufferType*>(mappedResource.pData);

    *lightBuffer = light.AsBuffer(static_cast<float>(lightCount));

    deviceContext.Unmap(m_lightBuffer.Get(), 0);
    deviceContext.PSSetConstantBuffers(1, 1, m_lightBuffer.GetAddressOf());

    // Update the light matrix cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_lightMatrixBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* lightMatrixBuffer = static_cast<PointLight::LightMatrixBufferType*>(mappedResource.pData);

    lightMatrixBuffer->viewMatrix = XMMatrixTranspose(light.GetViewMatrix());
    lightMatrixBuffer->projectionMatrix = XMMatrixTranspose(PointLight::GetProjectionMatrix());

    deviceContext.Unmap(m_lightMatrixBuffer.Get(), 0);
    deviceContext.PSSetConstantBuffers(2, 1, m_lightMatrixBuffer.GetAddressOf());

    // Bind the textures to the pixel shader.
    deviceContext.PSSetShaderResources(0, DeferredBuffers::BUFFER_COUNT, deferredBuffers.GetRenderTargets().data());
    deviceContext.PSSetShaderResources(DeferredBuffers::BUFFER_COUNT, 1, light.GetDepthMap().GetAddressOf());
}

void AspenRenderer::Shaders::DeferredLightShader::RenderShader(const int indexCount)
{
    // Set the input layout, shaders, samplers, and draw.
    auto& deviceContext = m_d3d->GetDeviceContext();
    deviceContext.IASetInputLayout(m_layout.Get());
    deviceContext.VSSetShader(m_vertexShader.Get(), nullptr, 0);
    deviceContext.PSSetShader(m_pixelShader.Get(), nullptr, 0);
    deviceContext.PSSetSamplers(0, 1, m_samplerState.GetAddressOf());
    deviceContext.DrawIndexed(indexCount, 0, 0);
}

void AspenRenderer::Shaders::DeferredLightShader::CompileShaders()
{
    auto& device = m_d3d->GetDevice();

    // Load the precompiled shaders.
    auto vertexShaderBuffer = LoadCompiledShader("DeferredLightVertexShader.cso");
    auto pixelShaderBuffer = LoadCompiledShader("DeferredLightPixelShader.cso");

    // Create the vertex shader from the buffer.
    DX::ThrowIfFailed(device.CreateVertexShader(
        vertexShaderBuffer.data(), vertexShaderBuffer.size(),
        nullptr, m_vertexShader.ReleaseAndGetAddressOf()
    ));

    // Create the pixel shader from the buffer.
    DX::ThrowIfFailed(device.CreatePixelShader(
        pixelShaderBuffer.data(), pixelShaderBuffer.size(),
        nullptr, m_pixelShader.ReleaseAndGetAddressOf())
    );

    // Create the vertex input layout description.
    std::vector<D3D11_INPUT_ELEMENT_DESC> polygonLayout = {
        {
            "POSITION",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            0,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TEXCOORD",
            0,
            DXGI_FORMAT_R32G32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        }
    };

    // Create the vertex input layout.
    DX::ThrowIfFailed(device.CreateInputLayout(
        polygonLayout.data(), 2, vertexShaderBuffer.data(),
        vertexShaderBuffer.size(), m_layout.ReleaseAndGetAddressOf())
    );
}

void AspenRenderer::Shaders::DeferredLightShader::InitializeSampler()
{
    // Create the sampler state description.
    D3D11_SAMPLER_DESC samplerDesc;
    samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 1;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 0;
    samplerDesc.BorderColor[1] = 0;
    samplerDesc.BorderColor[2] = 0;
    samplerDesc.BorderColor[3] = 0;
    samplerDesc.MinLOD = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

    // Create the sampler state.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateSamplerState(&samplerDesc, m_samplerState.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Shaders::DeferredLightShader::InitializeBuffers()
{
    // Create the matrix buffer.
    D3D11_BUFFER_DESC matrixBufferDesc;
    matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    matrixBufferDesc.ByteWidth = sizeof MatrixBufferType;
    matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    matrixBufferDesc.MiscFlags = 0;
    matrixBufferDesc.StructureByteStride = 0;

    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&matrixBufferDesc, nullptr, m_matrixBuffer.ReleaseAndGetAddressOf()));

    // Create the camera buffer.
    D3D11_BUFFER_DESC cameraBufferDesc;
    cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    cameraBufferDesc.ByteWidth = sizeof CameraBufferType;
    cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cameraBufferDesc.MiscFlags = 0;
    cameraBufferDesc.StructureByteStride = 0;

    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&cameraBufferDesc, nullptr, m_cameraBuffer.ReleaseAndGetAddressOf()));

    // Create the light buffer.
    D3D11_BUFFER_DESC lightBufferDesc;
    lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    lightBufferDesc.ByteWidth = sizeof PointLight::LightBufferType;
    lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    lightBufferDesc.MiscFlags = 0;
    lightBufferDesc.StructureByteStride = 0;

    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&lightBufferDesc, nullptr, m_lightBuffer.ReleaseAndGetAddressOf()));

    // Create the light matrix buffer.
    D3D11_BUFFER_DESC lightMatrixBufferDesc;
    lightMatrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    lightMatrixBufferDesc.ByteWidth = sizeof PointLight::LightMatrixBufferType;
    lightMatrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    lightMatrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    lightMatrixBufferDesc.MiscFlags = 0;
    lightMatrixBufferDesc.StructureByteStride = 0;

    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&lightMatrixBufferDesc, nullptr, m_lightMatrixBuffer.ReleaseAndGetAddressOf()));
}
