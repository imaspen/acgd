#pragma once
#include "Source/AspenRenderer/Shaders/Shader.h"

namespace AspenRenderer
{
    namespace Shaders
    {
        class MirrorShader : public TextureShader
        {
            struct ReflectionBufferType
            {
                DirectX::XMFLOAT4X4 reflectionViewMatrix;
            };

        public:
            explicit MirrorShader(std::shared_ptr<D3DClass> d3d);

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix,
                const DirectX::XMMATRIX& projectionMatrix, const DirectX::XMMATRIX& reflectionMatrix,
                ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* reflectionTexture);

        protected:
            void InitializeReflectionBuffers();

            Microsoft::WRL::ComPtr<ID3D11Buffer> m_reflectionBuffer;
        };
    }
}
