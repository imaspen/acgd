#include "pch.h"
#include "MirrorShader.h"

AspenRenderer::Shaders::MirrorShader::MirrorShader(std::shared_ptr<D3DClass> d3d) :
    TextureShader(std::move(d3d), "MirrorVertexShader.cso", "MirrorPixelShader.cso")
{
    InitializeReflectionBuffers();
}

void AspenRenderer::Shaders::MirrorShader::SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix, 
    const DirectX::XMMATRIX& projectionMatrix, const DirectX::XMMATRIX& reflectionMatrix,  ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* reflectionTexture)
{
    TextureShader::SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, colorTexture);

    D3D11_MAPPED_SUBRESOURCE mappedResource{};

    // Update the reflection cbuffer.
    DX::ThrowIfFailed(m_d3d->GetDeviceContext().Map(m_reflectionBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* reflectionBuffer = static_cast<ReflectionBufferType*>(mappedResource.pData);

    XMStoreFloat4x4(&reflectionBuffer->reflectionViewMatrix, XMMatrixTranspose(reflectionMatrix));

    m_d3d->GetDeviceContext().Unmap(m_reflectionBuffer.Get(), 0);

    // Pass the reflection cbuffer to the GPU.
    m_d3d->GetDeviceContext().VSSetConstantBuffers(1, 1, m_reflectionBuffer.GetAddressOf());

    // Pass the reflection texture to the GPU.
    m_d3d->GetDeviceContext().PSSetShaderResources(1, 1, &reflectionTexture);
}

void AspenRenderer::Shaders::MirrorShader::InitializeReflectionBuffers()
{
    // Create the reflection buffer.
    D3D11_BUFFER_DESC reflectionBufferDesc{
        sizeof ReflectionBufferType,
        D3D11_USAGE_DYNAMIC,
        D3D11_BIND_CONSTANT_BUFFER,
        D3D11_CPU_ACCESS_WRITE,
        0,
        0
    };

    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&reflectionBufferDesc, nullptr, m_reflectionBuffer.ReleaseAndGetAddressOf()));
}
