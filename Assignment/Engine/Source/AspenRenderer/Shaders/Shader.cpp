#include "pch.h"
#include "Shader.h"

#include <fstream>

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::Shaders::Shader::Shader(std::shared_ptr<D3DClass> d3d, const std::string& vsPath, const std::string& psPath) :
    m_d3d(std::move(d3d))
{
    LoadCompiledShaders(vsPath, psPath);
    InitializeMatrixBuffer();
}

AspenRenderer::Shaders::Shader::Shader(std::shared_ptr<D3DClass> d3d) noexcept :
    m_d3d(std::move(d3d))
{
}

void AspenRenderer::Shaders::Shader::SetShaderParameters(const XMMATRIX& worldMatrix, const XMMATRIX& viewMatrix,
    const XMMATRIX& projectionMatrix)
{
    D3D11_MAPPED_SUBRESOURCE mappedResource{};
    auto& deviceContext = m_d3d->GetDeviceContext();

    // Update the matrix cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_matrixBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* dataPtr = static_cast<MatrixBufferType*>(mappedResource.pData);

    XMStoreFloat4x4(&dataPtr->worldMatrix, XMMatrixTranspose(worldMatrix));
    XMStoreFloat4x4(&dataPtr->viewMatrix, XMMatrixTranspose(viewMatrix));
    XMStoreFloat4x4(&dataPtr->projectionMatrix, XMMatrixTranspose(projectionMatrix));

    deviceContext.Unmap(m_matrixBuffer.Get(), 0);

    // Assign the matrix cbuffer to the vertex shader.
    deviceContext.VSSetConstantBuffers(0, 1, m_matrixBuffer.GetAddressOf());
}

void AspenRenderer::Shaders::Shader::RenderShader(const int vertexCount, const int instanceCount) const
{
    // Assign the input layout, shaders, and draw.
    m_d3d->GetDeviceContext().IASetInputLayout(m_layout.Get());
    m_d3d->GetDeviceContext().VSSetShader(m_vertexShader.Get(), nullptr, 0);
    m_d3d->GetDeviceContext().PSSetShader(m_pixelShader.Get(), nullptr, 0);
    m_d3d->GetDeviceContext().DrawIndexedInstanced(vertexCount, instanceCount, 0, 0, 0);
}

std::vector<char> AspenRenderer::Shaders::Shader::LoadCompiledShader(const std::string& path)
{
    std::vector<char> data;

    // Open a file in binary read mode at the file end.
    std::ifstream file("Shaders/" + path, std::ios::in | std::ios::binary | std::ios::ate);

    // If the file opened,
    if (file.is_open())
    {
        // Set the length as the position of the final character.
        const auto length = static_cast<int>(file.tellg());

        // Fill the data vector with the contents of the file.
        data.resize(length);
        file.seekg(0, std::ios::beg);
        file.read(data.data(), length);
        file.close();
    }

    return data;
}

void AspenRenderer::Shaders::Shader::LoadCompiledShaders(const std::string& vsPath, const std::string& psPath)
{
    auto& device = m_d3d->GetDevice();

    // Load precompiled shaders.
    auto vertexShaderBuffer = LoadCompiledShader(vsPath);
    auto pixelShaderBuffer = LoadCompiledShader(psPath);

    // Create the vertex shader from the buffer.
    DX::ThrowIfFailed(device.CreateVertexShader(
        vertexShaderBuffer.data(), vertexShaderBuffer.size(),
        nullptr, m_vertexShader.ReleaseAndGetAddressOf()
    ));

    // Create the pixel shader from the buffer.
    DX::ThrowIfFailed(device.CreatePixelShader(
        pixelShaderBuffer.data(), pixelShaderBuffer.size(),
        nullptr, m_pixelShader.ReleaseAndGetAddressOf()
    ));

    // Create the vertex input layout description.
    std::vector<D3D11_INPUT_ELEMENT_DESC> polygonLayout{
        {
            "POSITION",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            0,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TEXCOORD",
            0,
            DXGI_FORMAT_R32G32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "NORMAL",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TANGENT",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "BINORMAL",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TEXCOORD",
            1,
            DXGI_FORMAT_R32G32B32_FLOAT,
            1,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_INSTANCE_DATA,
            1
        },
        {
            "TEXCOORD",
            2,
            DXGI_FORMAT_R32G32B32_FLOAT,
            1,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_INSTANCE_DATA,
            1
        }
    };

    // Create the vertex input layout.
    DX::ThrowIfFailed(device.CreateInputLayout(polygonLayout.data(), 7, vertexShaderBuffer.data(),
        vertexShaderBuffer.size(), m_layout.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Shaders::Shader::InitializeMatrixBuffer()
{
    // Create the matrix buffer description.
    D3D11_BUFFER_DESC matrixBufferDesc;
    matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    matrixBufferDesc.ByteWidth = sizeof MatrixBufferType;
    matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    matrixBufferDesc.MiscFlags = 0;
    matrixBufferDesc.StructureByteStride = 0;

    // Create the matrix buffer.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&matrixBufferDesc, nullptr, m_matrixBuffer.ReleaseAndGetAddressOf()));
}

AspenRenderer::Shaders::DepthShader::DepthShader(std::shared_ptr<D3DClass> d3d) :
    Shader(std::move(d3d))
{
    LoadCompiledShaders("ShadowMapVertexShader.cso", "ShadowMapPixelShader.cso");
    InitializeMatrixBuffer();
}

AspenRenderer::Shaders::TextureShader::TextureShader(std::shared_ptr<D3DClass> d3d, const std::string& vsPath, const std::string& psPath) :
    Shader(std::move(d3d), vsPath, psPath)
{
    InitializeSampler();
}

void AspenRenderer::Shaders::TextureShader::SetShaderParameters(const XMMATRIX& worldMatrix, const XMMATRIX& viewMatrix,
    const XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture)
{
    Shader::SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix);

    // Assign the texture and sampler to the pixel shader.
    m_d3d->GetDeviceContext().PSSetShaderResources(0, 1, &colorTexture);
    m_d3d->GetDeviceContext().PSSetSamplers(0, 1, m_samplerState.GetAddressOf());
}

void AspenRenderer::Shaders::TextureShader::InitializeSampler()
{
    // Create the sampler state description.
    D3D11_SAMPLER_DESC samplerDesc;
    samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 16;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 0;
    samplerDesc.BorderColor[1] = 0;
    samplerDesc.BorderColor[2] = 0;
    samplerDesc.BorderColor[3] = 0;
    samplerDesc.MinLOD = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

    // Create the sampler state.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateSamplerState(&samplerDesc, m_samplerState.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Shaders::DeferredBumpMapShader::SetShaderParameters(const XMMATRIX& worldMatrix, const XMMATRIX& viewMatrix, 
    const XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* bumpMap)
{
    TextureShader::SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, colorTexture);
    m_d3d->GetDeviceContext().PSSetShaderResources(1, 1, &bumpMap);
}

void AspenRenderer::Shaders::DeferredPBRShader::SetShaderParameters(const XMMATRIX& worldMatrix, const XMMATRIX& viewMatrix, 
    const XMMATRIX& projectionMatrix, ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* bumpMap, ID3D11ShaderResourceView* glossMap)
{
    TextureShader::SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, colorTexture);
    m_d3d->GetDeviceContext().PSSetShaderResources(1, 1, &bumpMap);
    m_d3d->GetDeviceContext().PSSetShaderResources(2, 1, &glossMap);
}
