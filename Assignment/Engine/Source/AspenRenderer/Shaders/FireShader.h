#pragma once
#include "Shader.h"

namespace AspenRenderer
{
    namespace Shaders
    {
        class FireShader : public TextureShader
        {
            struct NoiseBufferType
            {
                float frameTime;
                DirectX::XMFLOAT3 scrollSpeeds;
                DirectX::XMFLOAT3 scales;
                float padding;
            };

            struct DistortionBufferType
            {
                DirectX::XMFLOAT2 distortion1;
                DirectX::XMFLOAT2 distortion2;
                DirectX::XMFLOAT2 distortion3;
                float distortionScale;
                float distortionBias;
            };

        public:
            explicit FireShader(const std::shared_ptr<D3DClass>& d3d);

            void SetShaderParameters(const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix, const
                DirectX::XMMATRIX& projectionMatrix, float frameTime, ID3D11ShaderResourceView* colorTexture,
                ID3D11ShaderResourceView* alphaTexture, ID3D11ShaderResourceView* noiseTexture);

        protected:
            Microsoft::WRL::ComPtr<ID3D11Buffer> m_noiseBuffer, m_distortionBuffer;
            Microsoft::WRL::ComPtr<ID3D11SamplerState> m_clampSamplerState;

        private:
            void CompileShaders();
            void InitializeFireBuffers();
            void InitializeClampSampler();
        };
    }
}
