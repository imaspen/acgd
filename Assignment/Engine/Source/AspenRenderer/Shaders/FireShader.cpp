#include "pch.h"
#include "FireShader.h"

#include <vector>

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::Shaders::FireShader::FireShader(const std::shared_ptr<D3DClass>& d3d) :
    TextureShader(d3d)
{
    CompileShaders();
    InitializeMatrixBuffer();
    InitializeFireBuffers();
    InitializeSampler();
    InitializeClampSampler();
}

void AspenRenderer::Shaders::FireShader::CompileShaders()
{
    auto& device = m_d3d->GetDevice();

    // Load precompiled shaders.
    auto vertexShaderBuffer = LoadCompiledShader("FireVertexShader.cso");
    auto pixelShaderBuffer = LoadCompiledShader("FirePixelShader.cso");

    // Create the vertex shader from the buffer.
    DX::ThrowIfFailed(device.CreateVertexShader(
        vertexShaderBuffer.data(), vertexShaderBuffer.size(), 
        nullptr, m_vertexShader.ReleaseAndGetAddressOf()
    ));

    // Create the pixel shader from the buffer.
    DX::ThrowIfFailed(device.CreatePixelShader(
        pixelShaderBuffer.data(), pixelShaderBuffer.size(), 
        nullptr, m_pixelShader.ReleaseAndGetAddressOf()
    ));

    // Create the vertex input layout description.
    std::vector<D3D11_INPUT_ELEMENT_DESC> polygonLayout {
        {
            "POSITION",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            0,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TEXCOORD",
            0,
            DXGI_FORMAT_R32G32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0
        },
        {
            "TEXCOORD",
            1,
            DXGI_FORMAT_R32G32B32_FLOAT,
            1,
            0,
            D3D11_INPUT_PER_INSTANCE_DATA,
            1
        },
        {
            "TEXCOORD",
            2,
            DXGI_FORMAT_R32G32B32_FLOAT,
            1,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_INSTANCE_DATA,
            1
        }
    };

    // Create the vertex input layout.
    DX::ThrowIfFailed(device.CreateInputLayout(
        polygonLayout.data(), 4, vertexShaderBuffer.data(),
        vertexShaderBuffer.size(), m_layout.ReleaseAndGetAddressOf()
    ));
}

void AspenRenderer::Shaders::FireShader::InitializeFireBuffers()
{
    // Create the noise buffer description.
    D3D11_BUFFER_DESC noiseBufferDesc;
    noiseBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    noiseBufferDesc.ByteWidth = sizeof NoiseBufferType;
    noiseBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    noiseBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    noiseBufferDesc.MiscFlags = 0;
    noiseBufferDesc.StructureByteStride = 0;

    // Create the noise buffer.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&noiseBufferDesc, nullptr, m_noiseBuffer.ReleaseAndGetAddressOf()));

    // Create the distortion buffer description.
    D3D11_BUFFER_DESC distortionBufferDesc;
    distortionBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    distortionBufferDesc.ByteWidth = sizeof DistortionBufferType;
    distortionBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    distortionBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    distortionBufferDesc.MiscFlags = 0;
    distortionBufferDesc.StructureByteStride = 0;

    // Create the distortion buffer.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateBuffer(&distortionBufferDesc, nullptr, m_distortionBuffer.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Shaders::FireShader::InitializeClampSampler()
{
    // Create the clamp sampler state description.
    D3D11_SAMPLER_DESC samplerDesc;
    samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
    samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
    samplerDesc.MipLODBias = 0.0f;
    samplerDesc.MaxAnisotropy = 16;
    samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    samplerDesc.BorderColor[0] = 0;
    samplerDesc.BorderColor[1] = 0;
    samplerDesc.BorderColor[2] = 0;
    samplerDesc.BorderColor[3] = 0;
    samplerDesc.MinLOD = 0;
    samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

    // Create the clamp sampler state.
    DX::ThrowIfFailed(m_d3d->GetDevice().CreateSamplerState(&samplerDesc, m_clampSamplerState.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Shaders::FireShader::SetShaderParameters(const XMMATRIX& worldMatrix, const XMMATRIX& viewMatrix, const XMMATRIX& projectionMatrix, const float frameTime,
    ID3D11ShaderResourceView* colorTexture, ID3D11ShaderResourceView* alphaTexture, ID3D11ShaderResourceView* noiseTexture)
{
    TextureShader::SetShaderParameters(worldMatrix, viewMatrix, projectionMatrix, colorTexture);

    // Set the fire parameters used to perturb the texture.
    static XMFLOAT3 scrollSpeeds(1.3f, 2.1f, 2.3f);
    static XMFLOAT3 scales(1.0f, 2.0f, 3.0f);
    static XMFLOAT2 distortion1(0.1f, 0.2f);
    static XMFLOAT2 distortion2(0.1f, 0.3f);
    static XMFLOAT2 distortion3(0.1f, 0.1f);
    static auto distortionScale = 0.8f;
    static auto distortionBias = 0.5f;

    D3D11_MAPPED_SUBRESOURCE mappedResource{};
    auto& deviceContext = m_d3d->GetDeviceContext();

    // Update the noise cbuffer
    DX::ThrowIfFailed(deviceContext.Map(m_noiseBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* noiseBuffer = static_cast<NoiseBufferType*>(mappedResource.pData);
    noiseBuffer->frameTime = frameTime;
    noiseBuffer->scrollSpeeds = scrollSpeeds;
    noiseBuffer->scales = scales;

    deviceContext.Unmap(m_noiseBuffer.Get(), 0);

    // Update the distortion cbuffer.
    DX::ThrowIfFailed(deviceContext.Map(m_distortionBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource));

    auto* distortionBuffer = static_cast<DistortionBufferType*>(mappedResource.pData);
    distortionBuffer->distortion1 = distortion1;
    distortionBuffer->distortion2 = distortion2;
    distortionBuffer->distortion3 = distortion3;
    distortionBuffer->distortionScale = distortionScale;
    distortionBuffer->distortionBias = distortionBias;

    deviceContext.Unmap(m_distortionBuffer.Get(), 0);

    // Assign the constant buffers to the shaders.
    deviceContext.VSSetConstantBuffers(1, 1, m_noiseBuffer.GetAddressOf());
    deviceContext.PSSetConstantBuffers(0, 1, m_distortionBuffer.GetAddressOf());

    // Assign the texture resources to the pixel shader.
    deviceContext.PSSetShaderResources(1, 1, &noiseTexture);
    deviceContext.PSSetShaderResources(2, 1, &alphaTexture);

    // Assign the clamp sampler to the pixel shader.
    deviceContext.PSSetSamplers(1, 1, m_clampSamplerState.GetAddressOf());
}
