#include "pch.h"
#include "PointLight.h"

using namespace DirectX;

AspenRenderer::PointLight::PointLight(ID3D11Device& device) :
    PointLight(device, { 0.0f, 0.0f, 0.0f }, { 0.25f, 0.25f, 0.25f }, { 1.0f, 1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f }, 5.0f, 5.0f, 32.0f, ShadowDirection::NegativeY)
{
}

AspenRenderer::PointLight::PointLight(ID3D11Device& device, const XMFLOAT3& position,
    const XMFLOAT3& ambientColor, const XMFLOAT3& diffuseColor, const XMFLOAT3& specularColor,
    const float diffusePower, const float specularPower, const float specularHardness, ShadowDirection shadowDirection) :
    m_position(position),
    m_ambientColor(ambientColor),
    m_diffuseColor(diffuseColor),
    m_specularColor(specularColor),
    m_diffusePower(diffusePower),
    m_specularPower(specularPower),
    m_specularHardness(specularHardness),
    m_shadowDirection(shadowDirection)
{
    // Setup the texture description.
    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof textureDesc);
    textureDesc.Width = GetViewportSize();
    textureDesc.Height = GetViewportSize();
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    // Setup the render target view description.
    D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    ZeroMemory(&renderTargetViewDesc, sizeof renderTargetViewDesc);
    renderTargetViewDesc.Format = textureDesc.Format;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D.MipSlice = 0;

    // Setup the description of the shader resource view.
    D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
    ZeroMemory(&shaderResourceViewDesc, sizeof shaderResourceViewDesc);
    shaderResourceViewDesc.Format = textureDesc.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MipLevels = 1;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;

    // Create the texture and its views.
    DX::ThrowIfFailed(device.CreateTexture2D(&textureDesc, nullptr, m_texture.ReleaseAndGetAddressOf()));

    DX::ThrowIfFailed(device.CreateRenderTargetView(m_texture.Get(), &renderTargetViewDesc, m_renderTarget.ReleaseAndGetAddressOf()));

    DX::ThrowIfFailed(device.CreateShaderResourceView(m_texture.Get(), &shaderResourceViewDesc, m_shaderResource.ReleaseAndGetAddressOf()));

    // Set up the depth buffer.
    D3D11_TEXTURE2D_DESC depthBufferDesc;
    ZeroMemory(&depthBufferDesc, sizeof depthBufferDesc);
    depthBufferDesc.Width = GetViewportSize();
    depthBufferDesc.Height = GetViewportSize();
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    DX::ThrowIfFailed(device.CreateTexture2D(&depthBufferDesc, nullptr, m_depthStencilBuffer.ReleaseAndGetAddressOf()));

    // Set up the depth stencil view description.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(&depthStencilViewDesc, sizeof depthStencilViewDesc);
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    DX::ThrowIfFailed(device.CreateDepthStencilView(m_depthStencilBuffer.Get(), &depthStencilViewDesc, m_depthStencilView.ReleaseAndGetAddressOf()));
}

AspenRenderer::PointLight::LightBufferType AspenRenderer::PointLight::AsBuffer(const float lightCount) const noexcept
{
    // Return the light formatted for the shader cbuffer.
    return LightBufferType{
        m_position,
        m_specularHardness,
        m_ambientColor,
        lightCount,
        m_diffuseColor,
        m_diffusePower,
        m_specularColor,
        m_specularPower
    };
}

XMMATRIX AspenRenderer::PointLight::GetViewMatrix() const
{
    // Get the view matrix for the depth shader.
    const auto forward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
    const auto up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    const auto position = XMLoadFloat3(&m_position);

    // Get the view matrix for the direction the light casts shadows in.
    switch (m_shadowDirection)
    {
    case ShadowDirection::NegativeX:
        return XMMatrixLookToLH(position, XMVectorSet(-1.0f, 0.0f, 0.0f, 0.0f), up);
    case ShadowDirection::PositiveX:
        return XMMatrixLookToLH(position, XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f), up);
    case ShadowDirection::NegativeY:
        return XMMatrixLookToLH(position, XMVectorSet(0.0f, -1.0f, 0.0f, 0.0f), forward);
    case ShadowDirection::PositiveY:
        return XMMatrixLookToLH(position, XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f), forward);
    case ShadowDirection::NegativeZ:
        return XMMatrixLookToLH(position, XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f), up);
    case ShadowDirection::PositiveZ:
    default:
        return XMMatrixLookToLH(position, XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f), up);
    }
}

XMMATRIX AspenRenderer::PointLight::GetProjectionMatrix()
{
    return XMMatrixPerspectiveFovLH(XM_PI / 1.5f, 1.0f, 1.0f, 25.0f);
}

void AspenRenderer::PointLight::SetRenderTarget(ID3D11DeviceContext& deviceContext)
{
    // Set our viewport and render target to be active and clear the texture and depth stencil.
    constexpr float colors[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

    deviceContext.RSSetViewports(1, &m_viewport);
    deviceContext.OMSetRenderTargets(1, m_renderTarget.GetAddressOf(), m_depthStencilView.Get());
    deviceContext.ClearRenderTargetView(m_renderTarget.Get(), static_cast<const float*>(colors));
    deviceContext.ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}
