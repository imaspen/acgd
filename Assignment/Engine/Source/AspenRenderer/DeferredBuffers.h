#pragma once

#include "Source/d3dclass.h"

namespace AspenRenderer
{
    class DeferredBuffers
    {
    public:
        static const int BUFFER_COUNT = 3;

        DeferredBuffers(std::shared_ptr<D3DClass> d3d, int textureWidth, int textureHeight);

        void SetRenderTargets();
        void ClearRenderTargets(float red, float green, float blue, float alpha);
        Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& GetDepthStencil() noexcept;

        std::vector<ID3D11ShaderResourceView*> GetRenderTargets();

    private:

        int m_textureWidth, m_textureHeight;

        std::shared_ptr<D3DClass> m_d3d;

        std::vector<Microsoft::WRL::ComPtr<ID3D11Texture2D>> m_textures;
        std::vector<Microsoft::WRL::ComPtr<ID3D11RenderTargetView>> m_renderTargetViews;
        std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> m_shaderResourceViews;
        
        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_depthStencilBuffer;
        Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView;
        D3D11_VIEWPORT m_viewport;
    };
}
