#include "pch.h"
#include "WindowModel.h"

using namespace DirectX;

AspenRenderer::Models::WindowModel::WindowModel(ID3D11Device& device, const int screenWidth, const int screenHeight)
{
	InitializeBuffers(device, screenWidth, screenHeight);
}

void AspenRenderer::Models::WindowModel::RenderBuffers(ID3D11DeviceContext& deviceContext)
{
    const auto strides = GetVertexBufferSize();
	constexpr unsigned int offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext.IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &strides, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext.IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void AspenRenderer::Models::WindowModel::InitializeBuffers(ID3D11Device& device, const int width, const int height)
{
	// Calculate the screen coordinates of the window.
	const auto left = static_cast<float>(width) / 2.0f * -1.0f;
	const auto right = left + static_cast<float>(width);
	const auto top = static_cast<float>(height) / 2.0f;
	const auto bottom = top - static_cast<float>(height);

	// Set the number of vertices in the vertex array.
	m_vertexCount = 6;

	// Set the number of indices in the index array.
	m_indexCount = m_vertexCount;

	// Create the vertex and indices array.
	std::vector<unsigned long> indices;
    indices.reserve(m_indexCount);

	// Load the vertex array with data.
	// First triangle.
	std::vector<VertexType> vertices = {
		// Tri 1
		{   // Top left.
			XMFLOAT3(left, top, 0.0f),
			XMFLOAT2(0.0f, 0.0f)
		},
		{   // Bottom right.
			XMFLOAT3(right, bottom, 0.0f),
			XMFLOAT2(1.0f, 1.0f)
		},
		{   // Bottom left.
			XMFLOAT3(left, bottom, 0.0f),
			XMFLOAT2(0.0f, 1.0f)
		},
		// Tri 2
		{   // Top left.
			XMFLOAT3(left, top, 0.0f),
			XMFLOAT2(0.0f, 0.0f)
		},
		{   // Top right.
			XMFLOAT3(right, top, 0.0f),
			XMFLOAT2(1.0f, 0.0f)
		},
		{   // Bottom right.
			XMFLOAT3(right, bottom, 0.0f),
			XMFLOAT2(1.0f, 1.0f)
		}
	};

	// Load the index array with data.
	for (auto i = 0; i < m_indexCount; i++) indices.emplace_back(i);

	// Set up the description of the vertex buffer.
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices.data();
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&vertexBufferDesc, &vertexData, m_vertexBuffer.ReleaseAndGetAddressOf()));

	// Set up the description of the index buffer.
	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = indices.data();
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&indexBufferDesc, &indexData, m_indexBuffer.ReleaseAndGetAddressOf()));
}
