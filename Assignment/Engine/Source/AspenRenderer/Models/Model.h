#pragma once
#include "Source/AspenRenderer/AABB.h"

namespace AspenRenderer
{
    namespace Models
    {
        class Model
        {
        protected:
            struct VertexType
            {
                DirectX::XMFLOAT3 position;
                DirectX::XMFLOAT2 texture;
                DirectX::XMFLOAT3 normal;
                DirectX::XMFLOAT3 tangent;
                DirectX::XMFLOAT3 binormal;
            };

        public:
            struct InstanceType
            {
                DirectX::XMFLOAT3 position;
                DirectX::XMFLOAT3 rotation;
            };

            Model(ID3D11Device& device, const std::string& modelPath);
            Model(ID3D11Device& device, const std::string& modelPath, const std::vector<InstanceType>& instances);
            Model(const Model&) = default;
            Model(Model&&) = default;
            virtual ~Model() = default;
            Model& operator= (const Model&) = default;
            Model& operator= (Model&&) = default;

            void SetTranslation(const DirectX::XMFLOAT3& translation) noexcept { m_translation = translation; }
            void SetRotation(const DirectX::XMFLOAT3& rotation) noexcept { m_rotation = rotation; }
            void SetScale(const DirectX::XMFLOAT3& scale) noexcept { m_scale = scale; }
            DirectX::XMMATRIX GetTransform() const;

            virtual void RenderBuffers(ID3D11DeviceContext& deviceContext);

            AABB m_aabb;
            int m_vertexCount = 0, m_instanceCount = 0, m_indexCount = 0;

        protected:
            Model() = default;
            virtual unsigned GetVertexBufferSize() const noexcept { return sizeof VertexType; }
            virtual unsigned GetInstanceBufferSize() const noexcept { return sizeof InstanceType; }

            DirectX::XMFLOAT3 m_translation{}, m_rotation{}, m_scale{ 1.0f, 1.0f, 1.0f };

            Microsoft::WRL::ComPtr<ID3D11Buffer> m_vertexBuffer, m_instanceBuffer, m_indexBuffer;

        private:
            mutable DirectX::XMFLOAT4X4 m_transformCache{};
            mutable bool m_transformCacheValid = false;

            std::vector<VertexType> LoadModel(const std::string& modelPath);
            void InitializeBuffers(ID3D11Device& device, const std::vector<VertexType>& model, const std::vector<InstanceType>& instances);
            void GenerateAABB(const std::vector<VertexType>& model, const std::vector<InstanceType>& instances);
        };
    }
}
