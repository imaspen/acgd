#include "pch.h"
#include "Model.h"

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::Models::Model::Model(ID3D11Device& device, const std::string& modelPath) :
    Model(device, modelPath, { {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}} })
{}

AspenRenderer::Models::Model::Model(ID3D11Device& device, const std::string& modelPath, const std::vector<InstanceType>& instances)
{
    const auto model = LoadModel(modelPath);
    InitializeBuffers(device, model, instances);
    GenerateAABB(model, instances);
}

XMMATRIX AspenRenderer::Models::Model::GetTransform() const
{
    // If the cache is valid, return the cached value, else recalculate the transform.
    if (m_transformCacheValid) return XMLoadFloat4x4(&m_transformCache);

    // Create the transform matrix.
    const auto transform = XMMatrixTranslationFromVector(XMLoadFloat3(&m_translation))
        * XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&m_rotation))
        * XMMatrixScalingFromVector(XMLoadFloat3(&m_scale));

    // Revalidate the cache.
    m_transformCacheValid = true;
    XMStoreFloat4x4(&m_transformCache, transform);

    return transform;
}

std::vector<AspenRenderer::Models::Model::VertexType> AspenRenderer::Models::Model::LoadModel(const std::string& modelPath)
{
    std::ifstream fin;
    char input = 0;

    fin.open(modelPath);
    if (fin.fail()) throw std::exception("Failed to open model.");

    // Read up to the value of vertex count.
    fin.get(input);
    while (input != ':') fin.get(input);

    // Read in the vertex count.
    fin >> m_vertexCount;

    // Set the number of indices to be the same as the vertex count.
    m_indexCount = m_vertexCount;

    // Create the model using the vertex count that was read in.
    auto model = std::vector<VertexType>(m_vertexCount);

    // Read up to the beginning of the data.
    fin.get(input);
    while (input != ':') fin.get(input);
    fin.get(input);
    fin.get(input);

    // Read in the vertex data.
    for (auto& vertex : model)
    {
        fin >> vertex.position.x >> vertex.position.y >> vertex.position.z;
        fin >> vertex.texture.x >> vertex.texture.y;
        fin >> vertex.normal.x >> vertex.normal.y >> vertex.normal.z;
    }

    // Calculate the vertex tangent and binormals.
    for (unsigned int i = 2; i < model.size(); i += 3)
    {
        auto& vertex1 = model.at(i - 2);
        auto& vertex2 = model.at(i - 1);
        auto& vertex3 = model.at(i);

        const auto vector1 = XMLoadFloat3(&vertex2.position) - XMLoadFloat3(&vertex1.position);
        const auto vector2 = XMLoadFloat3(&vertex3.position) - XMLoadFloat3(&vertex1.position);

        const XMFLOAT2 tuVector(vertex2.texture.x - vertex1.texture.x,
            vertex3.texture.x - vertex1.texture.x);
        const XMFLOAT2 tvVector(vertex2.texture.y - vertex1.texture.y,
            vertex3.texture.y - vertex1.texture.y);

        const auto denominator = 1.0f / (tuVector.x * tvVector.y - tuVector.y * tvVector.x);

        XMFLOAT3 tangent{};
        XMStoreFloat3(&tangent, XMVector3Normalize(
            (tvVector.y * vector1 - tvVector.x * vector2) * denominator
        ));

        XMFLOAT3 binormal{};
        XMStoreFloat3(&binormal, XMVector3Normalize(
            (tuVector.x * vector2 - tuVector.y * vector1) * denominator
        ));

        vertex1.tangent = tangent;
        vertex1.binormal = binormal;

        vertex2.tangent = tangent;
        vertex2.binormal = binormal;

        vertex3.tangent = tangent;
        vertex3.binormal = binormal;
    }

    // Close the model file.
    fin.close();

    return model;
}

void AspenRenderer::Models::Model::RenderBuffers(ID3D11DeviceContext& deviceContext)
{
    const unsigned int strides[2] = { this->GetVertexBufferSize(), this->GetInstanceBufferSize() };
    const unsigned int offsets[2] = { 0, 0 };
    ID3D11Buffer* bufferPointers[2] = { m_vertexBuffer.Get(), m_instanceBuffer.Get() };

    // Set the vertex buffer to active in the input assembler so it can be rendered.
    deviceContext.IASetVertexBuffers(0, 2, static_cast<ID3D11Buffer* const*>(bufferPointers), 
        static_cast<const unsigned int*>(strides), static_cast<const unsigned int*>(offsets));

    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext.IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
    deviceContext.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void AspenRenderer::Models::Model::InitializeBuffers(ID3D11Device& device, const std::vector<VertexType>& model, const std::vector<InstanceType>& instances)
{
    m_instanceCount = instances.size();

    auto indices = std::vector<unsigned long>();
    indices.reserve(m_indexCount);
    for (auto i = 0; i < m_vertexCount; ++i) indices.push_back(i);

    // Set up the description of the static vertex buffer.
    D3D11_BUFFER_DESC vertexBufferDesc;
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
    vertexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the vertex data
    D3D11_SUBRESOURCE_DATA vertexData;
    vertexData.pSysMem = model.data();
    vertexData.SysMemPitch = 0;
    vertexData.SysMemSlicePitch = 0;

    // Now create the vertex buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&vertexBufferDesc, &vertexData, m_vertexBuffer.ReleaseAndGetAddressOf()));

    // Set up the description of the instance buffer.
    D3D11_BUFFER_DESC instanceBufferDesc;
    instanceBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    instanceBufferDesc.ByteWidth = sizeof(InstanceType) * m_instanceCount;
    instanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    instanceBufferDesc.CPUAccessFlags = 0;
    instanceBufferDesc.MiscFlags = 0;
    instanceBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the instance data
    D3D11_SUBRESOURCE_DATA instanceData;
    instanceData.pSysMem = instances.data();
    instanceData.SysMemPitch = 0;
    instanceData.SysMemSlicePitch = 0;

    // Now create the instance buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&instanceBufferDesc, &instanceData, m_instanceBuffer.ReleaseAndGetAddressOf()));

    // Set up the description of the static index buffer.
    D3D11_BUFFER_DESC indexBufferDesc;
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
    indexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the index data.
    D3D11_SUBRESOURCE_DATA indexData;
    indexData.pSysMem = indices.data();
    indexData.SysMemPitch = 0;
    indexData.SysMemSlicePitch = 0;

    // Create the index buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&indexBufferDesc, &indexData, m_indexBuffer.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Models::Model::GenerateAABB(const std::vector<VertexType>& model, const std::vector<InstanceType>& instances)
{
    auto minimums = XMVectorSplatInfinity();
    auto maximums = -XMVectorSplatInfinity();

    // Find the minimum and maximum x, y, and z values from all the vertices.
    for (const auto& instance : instances)
    {
        const auto translationMatrix = XMMatrixTranslationFromVector(XMLoadFloat3(&instance.position));
        const auto rotationMatrix = XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&instance.rotation));
        const auto transformationMatrix = rotationMatrix * translationMatrix;
        for (const auto& vertex : model)
        {
            const auto transformedVertex = XMVector3TransformCoord(XMLoadFloat3(&vertex.position), transformationMatrix);
            minimums = XMVectorMin(minimums, transformedVertex);
            maximums = XMVectorMax(maximums, transformedVertex);
        }
    }

    // Store the AABB.
    AABB aabb;
    XMStoreFloat3(&aabb.minimums, minimums);
    XMStoreFloat3(&aabb.maximums, maximums);
    m_aabb = aabb;
}
