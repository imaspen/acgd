#pragma once

#include "Model.h"

namespace AspenRenderer
{
    namespace Models
    {
        class WindowModel final : public Model
        {
            struct VertexType
            {
                DirectX::XMFLOAT3 position;
                DirectX::XMFLOAT2 texture;
            };

        public:
            explicit WindowModel(ID3D11Device& device, int screenWidth, int screenHeight);

            void RenderBuffers(ID3D11DeviceContext& deviceContext) override;

            unsigned GetVertexBufferSize() const noexcept override { return sizeof VertexType; }

        private:
            void InitializeBuffers(ID3D11Device& device, int width, int height);
        };
    }
}
