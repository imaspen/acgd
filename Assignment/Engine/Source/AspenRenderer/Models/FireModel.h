#pragma once

#include "Model.h"

namespace AspenRenderer
{
    namespace Models
    {
        class FireModel final : public Model
        {
            struct VertexType
            {
                DirectX::XMFLOAT3 position;
                DirectX::XMFLOAT2 texture;
            };

        public:
            explicit FireModel(ID3D11Device& device, const std::vector<InstanceType>& instances = {{{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}}});

            void RenderBuffers(ID3D11DeviceContext& deviceContext) override;

        protected:
            unsigned GetVertexBufferSize() const noexcept override;

        private:
            std::vector<VertexType> LoadModel();
            void InitializeBuffers(ID3D11Device& device, const std::vector<VertexType>& model, const std::vector<InstanceType>& instances);
        };
    }
}
