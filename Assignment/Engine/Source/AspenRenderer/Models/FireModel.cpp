#include "pch.h"
#include "FireModel.h"

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::Models::FireModel::FireModel(ID3D11Device& device, const std::vector<InstanceType>& instances)
{
    InitializeBuffers(device, LoadModel(), instances);
}

unsigned AspenRenderer::Models::FireModel::GetVertexBufferSize() const noexcept
{
    return sizeof VertexType;
}

std::vector<AspenRenderer::Models::FireModel::VertexType> AspenRenderer::Models::FireModel::LoadModel()
{
    std::ifstream fin;
    char input = 0;

    fin.open("data/Fire/Fire.txt");
    if (fin.fail()) throw std::exception("Failed to open model.");

    // Read up to the value of vertex count.
    fin.get(input);
    while (input != ':') fin.get(input);

    // Read in the vertex count.
    fin >> m_vertexCount;

    // Set the number of indices to be the same as the vertex count.
    m_indexCount = m_vertexCount;

    // Create the model using the vertex count that was read in.
    auto model = std::vector<VertexType>(m_vertexCount);

    // Read up to the beginning of the data.
    fin.get(input);
    while (input != ':') fin.get(input);
    fin.get(input);
    fin.get(input);

    // Read in the vertex data.
    for (auto& vertex : model)
    {
        fin >> vertex.position.x >> vertex.position.y >> vertex.position.z;
        fin >> vertex.texture.x >> vertex.texture.y;
    }

    // Close the model file.
    fin.close();

    return model;
}

void AspenRenderer::Models::FireModel::InitializeBuffers(ID3D11Device& device, const std::vector<VertexType>& model, const std::vector<InstanceType>& instances)
{
    m_instanceCount = instances.size();

    auto indices = std::vector<unsigned long>();
    indices.reserve(m_indexCount);
    for (auto i = 0; i < m_vertexCount; ++i) indices.push_back(i);

    // Set up the description of the static vertex buffer.
    D3D11_BUFFER_DESC vertexBufferDesc;
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
    vertexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the vertex data
    D3D11_SUBRESOURCE_DATA vertexData;
    vertexData.pSysMem = model.data();
    vertexData.SysMemPitch = 0;
    vertexData.SysMemSlicePitch = 0;

    // Now create the vertex buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&vertexBufferDesc, &vertexData, m_vertexBuffer.ReleaseAndGetAddressOf()));

    // Set up the description of the instance buffer.
    D3D11_BUFFER_DESC instanceBufferDesc;
    instanceBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    instanceBufferDesc.ByteWidth = sizeof(InstanceType) * m_instanceCount;
    instanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    instanceBufferDesc.CPUAccessFlags = 0;
    instanceBufferDesc.MiscFlags = 0;
    instanceBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the instance data
    D3D11_SUBRESOURCE_DATA instanceData;
    instanceData.pSysMem = instances.data();
    instanceData.SysMemPitch = 0;
    instanceData.SysMemSlicePitch = 0;

    // Now create the instance buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&instanceBufferDesc, &instanceData, m_instanceBuffer.ReleaseAndGetAddressOf()));

    // Set up the description of the static index buffer.
    D3D11_BUFFER_DESC indexBufferDesc;
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
    indexBufferDesc.StructureByteStride = 0;

    // Give the subresource structure a pointer to the index data.
    D3D11_SUBRESOURCE_DATA indexData;
    indexData.pSysMem = indices.data();
    indexData.SysMemPitch = 0;
    indexData.SysMemSlicePitch = 0;

    // Create the index buffer.
    DX::ThrowIfFailed(device.CreateBuffer(&indexBufferDesc, &indexData, m_indexBuffer.ReleaseAndGetAddressOf()));
}

void AspenRenderer::Models::FireModel::RenderBuffers(ID3D11DeviceContext& deviceContext)
{
    const unsigned int strides[2] = { this->GetVertexBufferSize(), this->GetInstanceBufferSize() };
    const unsigned int offsets[2] = { 0, 0 };
    ID3D11Buffer* bufferPointers[2] = { m_vertexBuffer.Get(), m_instanceBuffer.Get() };

    // Set the vertex buffer to active in the input assembler so it can be rendered.
    deviceContext.IASetVertexBuffers(0, 2, static_cast<ID3D11Buffer* const*>(bufferPointers), static_cast<const unsigned int*>(strides), static_cast<const unsigned int*>(offsets));

    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext.IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
    deviceContext.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}
