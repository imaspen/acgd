#include "pch.h"
#include "Camera.h"

using namespace DirectX;

XMMATRIX AspenRenderer::Camera::GetViewMatrix() const
{
	// If our cache is valid, return it, else recalculate it.
	if (m_viewMatrixCacheValid) return XMLoadFloat4x4(&m_viewMatrixCache);

	// Setup the position of the camera in the world.
	const auto position = XMLoadFloat3(&m_position);

	// Setup the vector that points upwards.
	auto up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	// Setup where the camera is looking by default.
	auto lookAt = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

	// Create the rotation matrix from the yaw, pitch, and roll values.
	const auto rotationMatrix = XMMatrixRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	lookAt = XMVector3TransformCoord(lookAt, rotationMatrix);

	up = XMVector3TransformCoord(up, rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = position + lookAt;

	// Finally create the view matrix from the three updated vectors.
	const auto viewMatrix = XMMatrixLookAtLH(position, lookAt, up);
	XMStoreFloat4x4(&m_viewMatrixCache, viewMatrix);
	m_viewMatrixCacheValid = true;

	return viewMatrix;
}

void AspenRenderer::Camera::SetPosition(const XMVECTOR& position)
{
	// Invalidate the view matrix cache and update position.
	m_viewMatrixCacheValid = false;
	XMStoreFloat3(&m_position, position);
}

DirectX::XMVECTOR AspenRenderer::Camera::GetRotationInDegrees() const {
	auto rotation = m_rotation;
	rotation.x = DirectX::XMConvertToDegrees(rotation.x);
	rotation.y = DirectX::XMConvertToDegrees(rotation.y);
	rotation.z = DirectX::XMConvertToDegrees(rotation.z);
	return XMLoadFloat3(&rotation);
}

void AspenRenderer::Camera::SetRotation(const XMVECTOR& rotation)
{
	// Invalidate the view matrix cache and update rotation.
	m_viewMatrixCacheValid = false;
	XMStoreFloat3(&m_rotation, rotation);
	m_rotation.x = XMConvertToRadians(m_rotation.x);
	m_rotation.y = XMConvertToRadians(m_rotation.y);
	m_rotation.z = XMConvertToRadians(m_rotation.z);
}
