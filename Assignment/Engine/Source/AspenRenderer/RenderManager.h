#pragma once

#include "Camera.h"
#include "../d3dclass.h"
#include "Models/FireModel.h"
#include "Models/Model.h"
#include "Models/WindowModel.h"
#include "Shaders/DeferredLightShader.h"
#include "Shaders/FireShader.h"
#include "Shaders/MirrorShader.h"

namespace AspenRenderer
{
    namespace Shaders
    {
        class Shader;
    }
    namespace GameObjects
    {
        class MirrorGameObject;
    }

    class DeferredBuffers;

    class RenderManager
    {
        struct AlbedoRenderType
        {
            std::shared_ptr<Models::Model> model;
            DirectX::XMMATRIX transform;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture;
        };
        struct BumpMapRenderType
        {
            std::shared_ptr<Models::Model> model;
            DirectX::XMMATRIX transform;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap;
        };
        struct GlossMapRenderType
        {
            std::shared_ptr<Models::Model> model;
            DirectX::XMMATRIX transform;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap;
        };
        struct PBRRenderType
        {
            std::shared_ptr<Models::Model> model;
            DirectX::XMMATRIX transform;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap;
        };
        struct FireRenderType
        {
            std::shared_ptr<Models::FireModel> model;
            float frameTime;
            DirectX::XMMATRIX transform;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> alphaMap;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> noiseMap;
        };
        struct MirrorRenderType
        {
            std::shared_ptr<Models::Model> model;
            DirectX::XMMATRIX transform, viewMatrix;
            std::shared_ptr<DeferredBuffers> buffers;
            Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTarget;
            Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencil;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> colorTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> reflectionTexture;
        };

    public:
        RenderManager(std::shared_ptr<D3DClass> d3d, std::shared_ptr<Camera> camera, int screenWidth, int screenHeight);

        void RegisterAlbedoModel(std::shared_ptr<Models::Model> model, const DirectX::XMMATRIX& transform,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture);
        void RegisterBumpMapModel(std::shared_ptr<Models::Model> model, const DirectX::XMMATRIX& transform,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap);
        void RegisterGlossMapModel(std::shared_ptr<Models::Model> model, const DirectX::XMMATRIX& transform,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap);
        void RegisterPBRModel(std::shared_ptr<Models::Model> model, const DirectX::XMMATRIX& transform,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap);
        void RegisterFireModel(std::shared_ptr<Models::FireModel> model, 
            float frameTime, const DirectX::XMMATRIX& transform, 
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture, 
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> alphaMap, 
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> noiseMap);
        void RegisterMirrorModel(std::shared_ptr<Models::Model> model, const DirectX::XMMATRIX& transform,
            const DirectX::XMMATRIX& viewMatrix, std::shared_ptr<DeferredBuffers> buffers,
            Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTarget,
            Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencil,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> colorTexture,
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> reflectionTexture);

        void Render(unsigned int isolatedLight, float shaderMode);

    private:
        void RenderShadowDepthMaps();
        void RenderDeferredModels(DeferredBuffers& buffers, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix);
        void RenderGBuffers(unsigned int isolatedLight, float shaderMode);
        void RenderFires(const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix);
        void RenderMirrors(const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix, float shaderMode);

        static std::unique_ptr<RenderManager> m_instance;

        std::vector<AlbedoRenderType> m_albedoQueue;
        std::vector<BumpMapRenderType> m_bumpMapQueue;
        std::vector<GlossMapRenderType> m_glossMapQueue;
        std::vector<PBRRenderType> m_pbrQueue;
        std::vector<FireRenderType> m_fireQueue;
        std::vector<MirrorRenderType> m_mirrorQueue;

        std::shared_ptr<D3DClass> m_d3d;
        std::shared_ptr<Camera> m_camera;
        std::shared_ptr<DeferredBuffers> m_deferredBuffers;

        std::vector<std::shared_ptr<PointLight>> m_lights;

        std::unique_ptr<Shaders::DepthShader> m_depthShader;
        std::unique_ptr<Shaders::TextureShader> m_deferredAlbedoShader;
        std::unique_ptr<Shaders::DeferredBumpMapShader> m_deferredBumpMapShader;
        std::unique_ptr<Shaders::DeferredPBRShader> m_deferredPBRShader;
        std::unique_ptr<Shaders::DeferredLightShader> m_deferredLightShader;
        std::unique_ptr<Shaders::FireShader> m_fireShader;
        std::unique_ptr<Shaders::MirrorShader> m_mirrorShader;

        std::unique_ptr<Models::WindowModel> m_windowModel;
    };
}
