#pragma once

#include "Camera.h"
#include "GameObjects/ModelGameObject.h"
#include "Source/inputclass.h"
#include "Source/controllerinterface.h"

namespace AspenRenderer
{
    class Controller
    {
    public:
        Controller(std::shared_ptr<InputClass> input, std::shared_ptr<Camera> camera, std::shared_ptr<ControllerInterface> ci) noexcept :
            m_input(std::move(input)),
            m_camera(std::move(camera)),
            m_ci(std::move(ci))
        {}

        void SetPosition(const DirectX::XMVECTOR& position) const
        {
            m_camera->SetPosition(position);
            m_aabbCacheValid = false;
        }

        void Tick(const std::list<std::shared_ptr<GameObjects::ModelGameObject>>& physicsObjects, float deltaTime);

    private:
        AABB GetAABB() const;

        std::shared_ptr<InputClass> m_input;
        std::shared_ptr<Camera> m_camera;
        std::shared_ptr<ControllerInterface> m_ci;

        DirectX::XMFLOAT3 m_speed{};

        AABB m_aabb{ -0.25f, -1.9f, -0.25f, 0.25f, 0.0f, 0.25f };
        float m_aabbRadius = 0.2501f;

        mutable AABB m_aabbCache;
        mutable bool m_aabbCacheValid = false;
    };
}
