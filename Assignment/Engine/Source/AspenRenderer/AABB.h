#pragma once

namespace AspenRenderer
{
    struct AABB
    {
        AABB() noexcept :
            minimums(),
            maximums()
        {}

        AABB(const float minX, const float minY, const float minZ,
            const float maxX, const float maxY, const float maxZ) noexcept :
            minimums{ minX, minY, minZ },
            maximums{ maxX, maxY, maxZ }
        {}

        AABB(const DirectX::XMFLOAT3& minimums, const DirectX::XMFLOAT3& maximums) noexcept :
            minimums(minimums),
            maximums(maximums)
        {}

        AABB Transform(const DirectX::XMMATRIX& transform) const;

        DirectX::XMFLOAT3 minimums;
        DirectX::XMFLOAT3 maximums;
    };
}
