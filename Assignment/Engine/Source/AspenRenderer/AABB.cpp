#include "pch.h"
#include "AABB.h"

using namespace DirectX;

AspenRenderer::AABB AspenRenderer::AABB::Transform(const XMMATRIX& transform) const
{
    // Generate the vertices of the AABB.
    std::vector<XMVECTOR> vertices{
        XMVectorSet(minimums.x, minimums.y, minimums.z, 0.0f),
        XMVectorSet(minimums.x, minimums.y, maximums.z, 0.0f),
        XMVectorSet(minimums.x, maximums.y, minimums.z, 0.0f),
        XMVectorSet(minimums.x, maximums.y, maximums.z, 0.0f),
        XMVectorSet(maximums.x, minimums.y, minimums.z, 0.0f),
        XMVectorSet(maximums.x, minimums.y, maximums.z, 0.0f),
        XMVectorSet(maximums.x, maximums.y, minimums.z, 0.0f),
        XMVectorSet(maximums.x, maximums.y, maximums.z, 0.0f),
    };

    // Find the min and max of x, y, and z.
    auto minVertex = XMVectorSplatInfinity();
    auto maxVertex = -XMVectorSplatInfinity();
    for (auto& vertex : vertices)
    {
        vertex = XMVector3TransformCoord(vertex, transform);
        minVertex = XMVectorMin(minVertex, vertex);
        maxVertex = XMVectorMax(maxVertex, vertex);
    }

    // Store and return the outputs.
    AABB ret;
    XMStoreFloat3(&ret.minimums, minVertex);
    XMStoreFloat3(&ret.maximums, maxVertex);
    return ret;
}
