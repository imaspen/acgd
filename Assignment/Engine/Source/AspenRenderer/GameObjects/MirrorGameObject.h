#pragma once

#include "GameObjects.h"

namespace AspenRenderer
{
    namespace GameObjects
    {
        class MirrorGameObject final : public AlbedoGameObject
        {
        public:
            MirrorGameObject(const std::shared_ptr<D3DClass>& d3d, std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Camera> camera, std::shared_ptr<Models::Model> model, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture);

            void SetRenderTargets() const;

            DirectX::XMMATRIX GetViewMatrix() const;

            Models::Model GetModel() const noexcept { return *m_model; }

            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> GetResourceView() const noexcept { return m_reflectionShaderResource; }

            void Render() override;

        private:
            std::shared_ptr<Camera> m_camera;
            std::shared_ptr<DeferredBuffers> m_deferredBuffers;

            Microsoft::WRL::ComPtr<ID3D11Texture2D> m_reflectionTexture;
            Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_reflectionRenderTarget;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_reflectionShaderResource;
            Microsoft::WRL::ComPtr<ID3D11Texture2D> m_depthStencil;
            Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView;
        };
    }
}
