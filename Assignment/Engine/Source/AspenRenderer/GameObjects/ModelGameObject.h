#pragma once
#include "GameObject.h"
#include "Source/AspenRenderer/RenderManager.h"

namespace AspenRenderer
{
    namespace GameObjects
    {
        class ModelGameObject : public GameObject, public std::enable_shared_from_this<ModelGameObject>
        {
        public:
            enum class PhysicsType
            {
                None,
                Static,
                Kinematic,
                Dynamic
            };

            ModelGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model) :
                m_renderManager(std::move(renderManager)),
                m_model(std::move(model))
            {}

            void SetLocalPosition(const DirectX::XMVECTOR& position) override
            {
                GameObject::SetLocalPosition(position);
                m_aabbCacheValid = false;
            }

            void SetLocalRotation(const DirectX::XMVECTOR& rotation) override
            {
                GameObject::SetLocalRotation(rotation);
                m_aabbCacheValid = false;
            }

            void SetLocalScale(const DirectX::XMVECTOR& scale) override
            {
                GameObject::SetLocalScale(scale);
                m_aabbCacheValid = false;
            }

            AABB GetAABB() const;

            void PhysicsTick(const std::list<std::shared_ptr<ModelGameObject>>& otherObjects, float deltaTime);

            std::list<std::shared_ptr<class ModelGameObject>> GetPhysicsObjects() override;

            PhysicsType m_physicsType = PhysicsType::Static;
            DirectX::XMFLOAT3 m_acceleration{}, m_speed{};

        protected:
            std::shared_ptr<RenderManager> m_renderManager;
            std::shared_ptr<Models::Model> m_model;

        private:
            mutable AABB m_aabbCache{};
            mutable bool m_aabbCacheValid = false;
        };
    }
}
