#pragma once

#include "GameObject.h"
#include "Source/AspenRenderer/RenderManager.h"

namespace AspenRenderer
{
    namespace GameObjects
    {
        class RootGameObject final : public GameObject
        {
        public:
            RootGameObject(const std::shared_ptr<RenderManager>& renderManager, const std::shared_ptr<D3DClass>& d3d, std::shared_ptr<Camera> camera);
        };
    }
}
