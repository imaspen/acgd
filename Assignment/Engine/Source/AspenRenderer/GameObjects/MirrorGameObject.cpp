#include "pch.h"
#include "MirrorGameObject.h"

using namespace DirectX;

AspenRenderer::GameObjects::MirrorGameObject::MirrorGameObject(const std::shared_ptr<D3DClass>& d3d, std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Camera> camera, std::shared_ptr<Models::Model> model, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture) :
    AlbedoGameObject(std::move(renderManager), std::move(model), std::move(albedoTexture)),
    m_camera(std::move(camera))
{
    auto& device = d3d->GetDevice();

    // Get the viewport.
    D3D11_VIEWPORT viewport{};
    UINT viewportCount = 1;
    d3d->GetDeviceContext().RSGetViewports(&viewportCount, &viewport);
    const auto width = static_cast<UINT>(viewport.Width);
    const auto height = static_cast<UINT>(viewport.Height);

    // Create deferred buffers.
    m_deferredBuffers = std::make_shared<DeferredBuffers>(d3d, width, height);

    // Create texture.
    D3D11_TEXTURE2D_DESC textureDesc{
        width,
        height,
        1,
        1,
        DXGI_FORMAT_R32G32B32A32_FLOAT,
        DXGI_SAMPLE_DESC{1, 0},
        D3D11_USAGE_DEFAULT,
        D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE,
        0,
        0
    };

    DX::ThrowIfFailed(device.CreateTexture2D(&textureDesc, nullptr, m_reflectionTexture.ReleaseAndGetAddressOf()));

    // Create render target view.
    D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    renderTargetViewDesc.Format = textureDesc.Format;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D = { 0 };

    DX::ThrowIfFailed(device.CreateRenderTargetView(m_reflectionTexture.Get(), &renderTargetViewDesc, m_reflectionRenderTarget.ReleaseAndGetAddressOf()));

    // Create shader resource view.
    D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
    shaderResourceViewDesc.Format = textureDesc.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D = { 0, 1 };

    DX::ThrowIfFailed(device.CreateShaderResourceView(m_reflectionTexture.Get(), &shaderResourceViewDesc, m_reflectionShaderResource.ReleaseAndGetAddressOf()));

    // Create depth stencil.
    D3D11_TEXTURE2D_DESC depthStencilDesc{
        width,
        height,
        1,
        1,
        DXGI_FORMAT_D24_UNORM_S8_UINT,
        {1, 0},
        D3D11_USAGE_DEFAULT,
        D3D11_BIND_DEPTH_STENCIL,
        0,
        0
    };
    DX::ThrowIfFailed(device.CreateTexture2D(&depthStencilDesc, nullptr, m_depthStencil.ReleaseAndGetAddressOf()));

    // Create the depth stencil view.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(&depthStencilViewDesc, sizeof depthStencilViewDesc);
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;
    DX::ThrowIfFailed(device.CreateDepthStencilView(m_depthStencil.Get(), &depthStencilViewDesc, m_depthStencilView.ReleaseAndGetAddressOf()));
}

void AspenRenderer::GameObjects::MirrorGameObject::SetRenderTargets() const
{
    m_deferredBuffers->SetRenderTargets();
}

XMMATRIX AspenRenderer::GameObjects::MirrorGameObject::GetViewMatrix() const
{
    // Create a view matrix, mirrored along the x-axis at the point of the position.
    XMFLOAT3 camPosition{}, camRotation{};
    XMStoreFloat3(&camPosition, m_camera->GetPosition());
    XMStoreFloat3(&camRotation, m_camera->GetRotation());

    auto up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    auto lookAt = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
    const auto position = XMVectorSet(2.0f * m_relativePosition.x - camPosition.x, camPosition.y, camPosition.z, 0.0f);

    const auto rotation = XMMatrixRotationRollPitchYaw(camRotation.x, -camRotation.y, camRotation.z);

    lookAt = XMVector3TransformCoord(lookAt, rotation);
    up = XMVector3TransformCoord(up, rotation);
    lookAt = position + lookAt;

    return XMMatrixLookAtLH(position, lookAt, up);
}

void AspenRenderer::GameObjects::MirrorGameObject::Render()
{
    ModelGameObject::Render();

    m_renderManager->RegisterMirrorModel(m_model, GetGlobalTransform(), GetViewMatrix(), 
        m_deferredBuffers, m_reflectionRenderTarget, m_depthStencilView, m_albedoTexture, m_reflectionShaderResource);
}
