#pragma once

namespace AspenRenderer
{
    namespace GameObjects
    {
        //class ModelGameObject;

        class GameObject
        {
        public:
            GameObject() = default;
            GameObject(const GameObject&) = default;
            GameObject(GameObject&&) = default;
            virtual ~GameObject() = default;
            GameObject& operator= (const GameObject&) = default;
            GameObject& operator= (GameObject&&) = default;

            // Tick ourself and our children.
            virtual void Tick(const float deltaTime) { for (const auto& child : m_children) child->Tick(deltaTime); }
            // Render ourself and our children.
            virtual void Render() { for (const auto& child : m_children) child->Render(); }

            void AddChild(const std::shared_ptr<GameObject>& gameObject);

            DirectX::XMVECTOR GetLocalPosition() const { return XMLoadFloat3(&m_relativePosition); }
            virtual void SetLocalPosition(const DirectX::XMVECTOR& position)
            {
                XMStoreFloat3(&m_relativePosition, position);
                m_transformCacheValid = false;
            }

            DirectX::XMVECTOR GetLocalRotation() const { return XMLoadFloat3(&m_relativeRotation); }
            virtual void SetLocalRotation(const DirectX::XMVECTOR& rotation)
            {
                XMStoreFloat3(&m_relativeRotation, rotation);
                m_transformCacheValid = false;
            }

            DirectX::XMVECTOR GetLocalScale() const { return XMLoadFloat3(&m_relativeScale); }
            virtual void SetLocalScale(const DirectX::XMVECTOR& scale)
            {
                XMStoreFloat3(&m_relativeScale, scale);
                m_transformCacheValid = false;
            }

            DirectX::XMMATRIX GetGlobalTransform() const;

            virtual std::list<std::shared_ptr<class ModelGameObject>> GetPhysicsObjects();

        protected:
            void InvalidateTransform();

            std::list<std::shared_ptr<GameObject>> m_children;
            GameObject* m_parent{};

            mutable DirectX::XMFLOAT4X4 m_transformCache{};
            mutable bool m_transformCacheValid = false;

            DirectX::XMFLOAT3 m_relativePosition{}, m_relativeRotation{}, m_relativeScale{ 1.0f, 1.0f, 1.0f };
        };
    }
}
