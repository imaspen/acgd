#include "pch.h"
#include "GameObject.h"

using namespace DirectX;

void AspenRenderer::GameObjects::GameObject::AddChild(const std::shared_ptr<GameObject>& gameObject)
{
    // Set the new child's parent to this and put it in the child list.
    gameObject->m_parent = this;
    m_children.emplace_back(gameObject);
}

XMMATRIX AspenRenderer::GameObjects::GameObject::GetGlobalTransform() const
{
    // If transform cache valid, return it, otherwise recalculate it.
    if (m_transformCacheValid) return XMLoadFloat4x4(&m_transformCache);

    // Create our local transform matrices.
    const auto translator = XMMatrixTranslationFromVector(GetLocalPosition());
    const auto rotator = XMMatrixRotationRollPitchYawFromVector(GetLocalRotation());
    const auto scaler = XMMatrixScalingFromVector(GetLocalScale());

    // Get our parents transform, if it exists.
    const auto parentTransform = m_parent == nullptr ? XMMatrixIdentity() : m_parent->GetGlobalTransform();

    // Generate the transform matrix.
    const auto transform = scaler * rotator * translator * parentTransform;

    // Revalidate the cache and return the transform.
    m_transformCacheValid = true;
    XMStoreFloat4x4(&m_transformCache, transform);
    return transform;
}

std::list<std::shared_ptr<AspenRenderer::GameObjects::ModelGameObject>> AspenRenderer::GameObjects::GameObject::GetPhysicsObjects()
{
    // Create an empty list.
    std::list<std::shared_ptr<ModelGameObject>> objects;
    for (const auto& child : m_children)
    {
        // Add the lists our children return to our list.
        objects.splice(objects.end(), child->GetPhysicsObjects());
    }
    return objects;
}

void AspenRenderer::GameObjects::GameObject::InvalidateTransform()
{
    // Invalidate our own cache and our child caches.
    m_transformCacheValid = false;
    for (const auto& child : m_children)
        child->InvalidateTransform();
}
