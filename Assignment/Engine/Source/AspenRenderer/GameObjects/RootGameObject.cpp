#include "pch.h"
#include "RootGameObject.h"

#include "MirrorGameObject.h"
#include "GameObjects.h"

using Microsoft::WRL::ComPtr;
using AspenRenderer::Models::Model;
using namespace DirectX;

AspenRenderer::GameObjects::RootGameObject::RootGameObject(const std::shared_ptr<RenderManager>& renderManager, const std::shared_ptr<D3DClass>& d3d, std::shared_ptr<Camera> camera)
{
    auto& device = d3d->GetDevice();

    // Create the front wall by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> frontWallInstances;
    frontWallInstances.reserve(10);
    for (auto i = 0; i < 5; ++i)
    {
        for (auto j = 0; j <= 2; ++j)
        {
            frontWallInstances.push_back({
                {2.0f * static_cast<float>(i - 2), 1.0f + 2.0f * static_cast<float>(j), 5.0f},
                {0.0f, 0.0f, 0.0f}
                });
        }
    }
    AddChild(std::make_shared<PBRGameObject>(renderManager, device, "data/wall/Wall", frontWallInstances));

    // Create the back wall by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> backWallInstances;
    backWallInstances.reserve(10);
    for (auto i = 0; i < 5; ++i)
    {
        for (auto j = 0; j <= 2; ++j)
        {
            backWallInstances.push_back({
                {2.0f * static_cast<float>(i - 2), 1.0f + 2.0f * static_cast<float>(j), -5.0f},
                {0.0f, XMConvertToRadians(180.0f), 0.0f}
                });
        }
    }
    AddChild(std::make_shared<PBRGameObject>(renderManager, device, "data/wall/Wall", backWallInstances));

    // Create the left wall by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> leftWallInstances;
    leftWallInstances.reserve(10);
    for (auto i = 0; i < 5; ++i)
    {
        for (auto j = 0; j <= 2; ++j)
        {
            leftWallInstances.push_back({
                {-5.0f, 1.0f + 2.0f * static_cast<float>(j), 2.0f * static_cast<float>(i - 2)},
                {0.0f, XMConvertToRadians(90.0f), 0.0f}
                });
        }
    }
    AddChild(std::make_shared<PBRGameObject>(renderManager, device, "data/wall/Wall", leftWallInstances));

    // Create the right wall by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> rightWallInstances;
    rightWallInstances.reserve(10);
    for (auto i = 0; i < 5; ++i)
    {
        for (auto j = 0; j <= 2; ++j)
        {
            rightWallInstances.push_back({
                {5.0f, 1.0f + 2.0f * static_cast<float>(j), 2.0f * static_cast<float>(i - 2)},
                {0.0f, XMConvertToRadians(270.0f), 0.0f}
                });
        }
    }
    AddChild(std::make_shared<PBRGameObject>(renderManager, device, "data/wall/Wall", rightWallInstances));

    // Create the floor by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> floorInstances;
    floorInstances.reserve(25);
    for (auto z = 0; z < 5; ++z)
    {
        for (auto x = 0; x < 5; ++x)
        {
            floorInstances.push_back({
                {2.0f * static_cast<float>(x - 2), 0.0f, 2.0f * static_cast<float>(z - 2)},
                {0.0f, 0.0f, 0.0f}
                });
        }
    }
    AddChild(std::make_shared<PBRGameObject>(renderManager, device, "data/floor/Floor", floorInstances));

    // Create the ceiling by creating a vector of instances and adding to the game object.
    std::vector<Model::InstanceType> ceilingInstances;
    ceilingInstances.reserve(15);
    for (auto z = 0; z < 5; ++z)
    {
        for (auto x = 0; x < 3; ++x)
        {
            ceilingInstances.push_back({
                {4.0f * static_cast<float>(x - 1), 0.0f, 2.0f * static_cast<float>(z - 2)},
                {}
                });
        }
    }
    const auto ceiling = std::make_shared<PBRGameObject>(renderManager, device, "data/Ceiling/Ceiling", ceilingInstances);
    ceiling->SetLocalPosition(XMVectorSet(0.0f, 6.0f, 0.0f, 0.0f));
    AddChild(ceiling);

    // Create the table and the objects on it.
    const auto table = std::make_shared<PBRGameObject>(renderManager, device, "data/table/Table.txt",
        L"data/table/Table_AlbedoTransparency.DDS", L"data/table/Table_Normal.DDS", L"data/table/Table_MetallicSmoothness.DDS");

    // Create the chairs, first create a chair instance and position it.
    auto chair = std::make_shared<PBRGameObject>(renderManager, device, "data/Chair/Chair");
    chair->SetLocalPosition(XMVectorSet(-0.7f, 0.0f, 1.5f, 0.0f));
    chair->SetLocalRotation(XMVectorSet(0.0f, XMConvertToRadians(80.0f), 0.0f, 0.0f));
    table->AddChild(chair);
    // Now copy the chair and move it.
    chair = std::make_shared<PBRGameObject>(*chair);
    chair->SetLocalPosition(XMVectorSet(0.6f, 0.0f, 1.5f, 0.0f));
    chair->SetLocalRotation(XMVectorSet(0.0f, XMConvertToRadians(105.0f), 0.0f, 0.0f));
    table->AddChild(chair);

    // Add the table now that everything has been added to it.
    AddChild(table);

    // Create the mirror.
    ComPtr<ID3D11ShaderResourceView> mirrorTextureView;
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, L"data/Mirror/Mirror.DDS", nullptr, mirrorTextureView.ReleaseAndGetAddressOf()));
    const auto mirror = std::make_shared<MirrorGameObject>(d3d, renderManager, std::move(camera), std::make_shared<Model>(device, "data/Mirror/Mirror.txt"), mirrorTextureView);
    mirror->SetLocalPosition(XMVectorSet(-4.99f, 2.0f, 0.0f, 0.0f));
    mirror->SetLocalRotation(XMVectorSet(0.0f, XMConvertToRadians(-90.0f), 0.0f, 0.0f));
    mirror->m_physicsType = ModelGameObject::PhysicsType::None;
    AddChild(mirror);

    // Create the fireplace.
    const auto fireplace = std::make_shared<PBRGameObject>(renderManager, device, "data/fireplace/Fireplace");
    fireplace->SetLocalPosition(XMVectorSet(0.0f, 0.0f, 4.0f, 0.0f));

    // Create the fire textures.
    ComPtr<ID3D11ShaderResourceView> albedoTexture, alphaMap, noiseMap;
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, L"data/Fire/Fire_Albedo.DDS", nullptr, albedoTexture.ReleaseAndGetAddressOf()));
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, L"data/Fire/Fire_Alpha.DDS", nullptr, alphaMap.ReleaseAndGetAddressOf()));
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, L"data/Fire/Fire_Noise.DDS", nullptr, noiseMap.ReleaseAndGetAddressOf()));

    // Create the fire and add it to the fireplace.
    const auto fire = std::make_shared<FireGameObject>(renderManager, std::make_shared<Models::FireModel>(device), albedoTexture, alphaMap, noiseMap);
    fire->m_physicsType = ModelGameObject::PhysicsType::None;
    fireplace->AddChild(fire);

    // Add the fireplace.
    AddChild(fireplace);

    // Create props around the room.
    // Create the barrel.
    const auto barrel = std::make_shared<PBRGameObject>(renderManager, device, "data/Barrel/Barrel");
    barrel->SetLocalPosition(XMVectorSet(-4.5f, 0.0f, -4.5f, 0.0f));
    barrel->SetLocalRotation(XMVectorSet(0.0f, XMConvertToRadians(225.0f), 0.0f, 0.0f));
    AddChild(barrel);

    // Create the cauldron.
    const auto cauldron = std::make_shared<PBRGameObject>(renderManager, device, "data/Cauldron/Cauldron");
    cauldron->SetLocalPosition(XMVectorSet(4.0f, 0.0f, 4.0f, 0.0f));
    cauldron->SetLocalRotation(XMVectorSet(0.0f, XMConvertToRadians(45.0f), 0.0f, 0.0f));
    AddChild(cauldron);

    // Create the pantry and the objects on it.
    const auto pantry = std::make_shared<PBRGameObject>(renderManager, device, "data/Pantry/Pantry");
    pantry->SetLocalPosition(XMVectorSet(4.0f, 0.0f, -4.5f, 0.0f));

    // Create the cheese.
    const auto cheese = std::make_shared<PBRGameObject>(renderManager, device, "data/Cheese/Cheese");
    cheese->SetLocalPosition(XMVectorSet(0.0f, 0.45f, 0.0f, 0.0f));
    cheese->m_physicsType = ModelGameObject::PhysicsType::None;
    pantry->AddChild(cheese);

    // Create the carrot.
    const auto carrot = std::make_shared<PBRGameObject>(renderManager, device, "data/Carrot/Carrot", std::vector<Model::InstanceType>{
        { {-0.4f, 0.0f, 0.0f}, { 0.0f, XMConvertToRadians(-30.0f), 0.0f }},
        { {0.2f, 0.0f, 0.0f}, {0.0f, XMConvertToRadians(70.0f), 0.0f} },
    });
    carrot->SetLocalPosition(XMVectorSet(0.0f, 0.8f, 0.0f, 0.0f));
    carrot->m_physicsType = ModelGameObject::PhysicsType::None;
    pantry->AddChild(carrot);

    // Create the apple.
    const auto apple = std::make_shared<PBRGameObject>(renderManager, device, "data/Apple/Apple", std::vector<Model::InstanceType>{
        { {-0.25f, 0.0f, 0.05f}, { 0.0f, XMConvertToRadians(30.0f), 0.0f }},
        { {0.0f, 0.0f, -0.05f}, {0.0f, XMConvertToRadians(-20.0f), 0.0f} },
        { {0.3f, 0.0f, 0.0f}, {0.0f, 0.0f , 0.0f} },
    });
    apple->SetLocalPosition(XMVectorSet(0.0f, 1.15f, 0.0f, 0.0f));
    apple->m_physicsType = ModelGameObject::PhysicsType::None;
    pantry->AddChild(apple);

    // Finally, add the pantry.
    AddChild(pantry);
}
