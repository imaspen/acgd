#include "pch.h"
#include "GameObjects.h"

using Microsoft::WRL::ComPtr;
using namespace DirectX;

AspenRenderer::GameObjects::AlbedoGameObject::AlbedoGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ID3D11Device& device, const std::wstring& albedoTexture) :
    ModelGameObject(std::move(renderManager), std::move(model))
{
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, albedoTexture.c_str(), nullptr, m_albedoTexture.ReleaseAndGetAddressOf()));
}

AspenRenderer::GameObjects::AlbedoGameObject::AlbedoGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ComPtr<ID3D11ShaderResourceView> albedoTexture) :
    ModelGameObject(std::move(renderManager), std::move(model)),
    m_albedoTexture(std::move(albedoTexture))
{}

void AspenRenderer::GameObjects::AlbedoGameObject::Render()
{
    m_renderManager->RegisterAlbedoModel(m_model, GetGlobalTransform(), m_albedoTexture);
    GameObject::Render();
}

AspenRenderer::GameObjects::BumpMapGameObject::BumpMapGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ComPtr<ID3D11ShaderResourceView> albedoTexture, ComPtr<ID3D11ShaderResourceView> bumpMap) :
    ModelGameObject(std::move(renderManager), std::move(model)),
    m_albedoTexture(std::move(albedoTexture)),
    m_bumpMap(std::move(bumpMap))
{}

void AspenRenderer::GameObjects::BumpMapGameObject::Render()
{
    m_renderManager->RegisterBumpMapModel(m_model, GetGlobalTransform(), m_albedoTexture, m_bumpMap);
    GameObject::Render();
}

AspenRenderer::GameObjects::GlossMapGameObject::GlossMapGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ComPtr<ID3D11ShaderResourceView> albedoTexture, ComPtr<ID3D11ShaderResourceView> glossMap) :
    ModelGameObject(std::move(renderManager), std::move(model)),
    m_albedoTexture(std::move(albedoTexture)),
    m_glossMap(std::move(glossMap))
{}

void AspenRenderer::GameObjects::GlossMapGameObject::Render()
{
    m_renderManager->RegisterGlossMapModel(m_model, GetGlobalTransform(), m_albedoTexture, m_glossMap);
    GameObject::Render();
}

AspenRenderer::GameObjects::PBRGameObject::PBRGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ComPtr<ID3D11ShaderResourceView> albedoTexture,
    ComPtr<ID3D11ShaderResourceView> bumpMap, ComPtr<ID3D11ShaderResourceView> glossMap) :
    ModelGameObject(std::move(renderManager), std::move(model)),
    m_albedoTexture(std::move(albedoTexture)),
    m_bumpMap(std::move(bumpMap)),
    m_glossMap(std::move(glossMap))
{}

AspenRenderer::GameObjects::PBRGameObject::PBRGameObject(std::shared_ptr<RenderManager> renderManager,
    ID3D11Device& device, const std::string& modelPath, const std::wstring& albedoPath, const std::wstring& bumpPath,
    const std::wstring& glossPath, const std::vector<Models::Model::InstanceType>& instances) :
    ModelGameObject(std::move(renderManager), std::make_shared<Models::Model>(device, modelPath, instances))
{
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, albedoPath.c_str(), nullptr, m_albedoTexture.ReleaseAndGetAddressOf()));
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, bumpPath.c_str(), nullptr, m_bumpMap.ReleaseAndGetAddressOf()));
    DX::ThrowIfFailed(CreateDDSTextureFromFile(&device, glossPath.c_str(), nullptr, m_glossMap.ReleaseAndGetAddressOf()));
}

AspenRenderer::GameObjects::PBRGameObject::PBRGameObject(std::shared_ptr<RenderManager> renderManager,
    ID3D11Device& device, const std::string& path, const std::vector<Models::Model::InstanceType>& instances) :
    PBRGameObject(std::move(renderManager), device, path + ".txt", DX::StringToWString(path) + L"_AlbedoTransparency.DDS",
        DX::StringToWString(path) + L"_Normal.DDS", DX::StringToWString(path) + L"_MetallicSmoothness.DDS", instances)
{}

void AspenRenderer::GameObjects::PBRGameObject::Render()
{
    m_renderManager->RegisterPBRModel(m_model, GetGlobalTransform(), m_albedoTexture, m_bumpMap, m_glossMap);
    GameObject::Render();
}

AspenRenderer::GameObjects::FireGameObject::FireGameObject(std::shared_ptr<RenderManager> renderManager,
    std::shared_ptr<Models::Model> model, ComPtr<ID3D11ShaderResourceView> albedoTexture,
    ComPtr<ID3D11ShaderResourceView> alphaMap, ComPtr<ID3D11ShaderResourceView> noiseMap) :
    ModelGameObject(std::move(renderManager), std::move(model)),
    m_albedoTexture(std::move(albedoTexture)),
    m_alphaMap(std::move(alphaMap)),
    m_noiseMap(std::move(noiseMap))
{}

void AspenRenderer::GameObjects::FireGameObject::Tick(const float deltaTime)
{
    m_frameTime += deltaTime;
    GameObject::Tick(deltaTime);
}

void AspenRenderer::GameObjects::FireGameObject::Render()
{
    m_renderManager->RegisterFireModel(std::dynamic_pointer_cast<Models::FireModel>(m_model), m_frameTime, GetGlobalTransform(), m_albedoTexture, m_alphaMap, m_noiseMap);
    GameObject::Render();
}
