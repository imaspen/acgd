#pragma once

#include "ModelGameObject.h"
#include "Source/AspenRenderer/RenderManager.h"


namespace AspenRenderer
{
    namespace GameObjects
    {
        class AlbedoGameObject : public ModelGameObject
        {
        public:
            AlbedoGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model, ID3D11Device& device, const std::wstring& albedoTexture);

            AlbedoGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture);

            void Render() override;

        protected:
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_albedoTexture;
        };


        class BumpMapGameObject final : public ModelGameObject
        {
        public:
            BumpMapGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model,
                Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
                Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap);

            void Render() override;

        protected:
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_bumpMap;
        };


        class GlossMapGameObject final : public ModelGameObject
        {
        public:
            GlossMapGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model,
                Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap);

            void Render() override;

        protected:
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_glossMap;
        };


        class PBRGameObject final : public ModelGameObject
        {
        public:
            PBRGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
                Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> bumpMap, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> glossMap);

            PBRGameObject(std::shared_ptr<RenderManager> renderManager, ID3D11Device& device, const std::string& modelPath,
                const std::wstring& albedoPath, const std::wstring& bumpPath, const std::wstring& glossPath,
                const std::vector<Models::Model::InstanceType>& instances = { {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}} });

            PBRGameObject(std::shared_ptr<RenderManager> renderManager, ID3D11Device& device, const std::string& path,
                const std::vector<Models::Model::InstanceType>& instances = { {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}} });

            void Render() override;

        protected:
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_bumpMap;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_glossMap;
        };

        class FireGameObject final : public ModelGameObject
        {
        public:
            FireGameObject(std::shared_ptr<RenderManager> renderManager, std::shared_ptr<Models::Model> model, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> albedoTexture,
                Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> alphaMap, Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> noiseMap);

            void Tick(float deltaTime) override;
            void Render() override;

        protected:
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_albedoTexture;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_alphaMap;
            Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_noiseMap;

        private:
            float m_frameTime = 0.0f;
        };
    }
}
