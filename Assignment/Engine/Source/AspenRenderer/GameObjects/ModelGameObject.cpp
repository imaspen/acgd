#include "pch.h"
#include "ModelGameObject.h"

using namespace DirectX;

AspenRenderer::AABB AspenRenderer::GameObjects::ModelGameObject::GetAABB() const
{
    // If our cache isn't valid, update it.
    if (!m_aabbCacheValid)
    {
        m_aabbCache = m_model->m_aabb.Transform(GetGlobalTransform());
        m_aabbCacheValid = true;
    }

    // Return the cached value.
    return m_aabbCache;
}

void AspenRenderer::GameObjects::ModelGameObject::PhysicsTick(const std::list<std::shared_ptr<ModelGameObject>>& otherObjects, const float deltaTime)
{
    // If we aren't of a movable physics type, do nothing.
    if (m_physicsType == PhysicsType::None || m_physicsType == PhysicsType::Static) return;

    // Get our initial speed and acceleration.
    const auto startSpeed = XMLoadFloat3(&m_speed);
    const auto startAcceleration = XMLoadFloat3(&m_acceleration);

    // If we aren't moving, do nothing.
    if (XMVector3Equal(startSpeed, XMVectorZero()) && XMVector3Equal(startAcceleration, XMVectorZero()))
        return;

    // Get our initial AABB.
    const auto startAABB = GetAABB();// m_model->m_aabb.Transform(GetGlobalTransform());

    // Update our speed and location.
    const auto newSpeed = startSpeed + startAcceleration * deltaTime;
    XMStoreFloat3(&m_speed, newSpeed);
    SetLocalPosition(GetLocalPosition() + newSpeed * deltaTime);

    // Get our new AABB.
    const auto newAABB = GetAABB();

    // If we're kinematic, don't do collision detection and response.
    if (m_physicsType == PhysicsType::Kinematic) return;

    // Iterate through the other objects.
    for (const auto& object : otherObjects)
    {
        // Don't collide with anything other than static objects.
        // None physics type doesn't collide and Dynamic and Kinematic are out of scope of RTG.
        if (object->m_physicsType != PhysicsType::Static)
            continue;

        // Don't collide with ourselves.
        if (object.get() == this)
            continue;

        // Get the AABB of the other object.
        const auto otherAABB = object->m_model->m_aabb.Transform(object->GetGlobalTransform());

        // If we're not colliding on any axis, we aren't colliding.
        if (newAABB.minimums.x > otherAABB.maximums.x) continue;
        if (newAABB.maximums.x < otherAABB.minimums.x) continue;
        if (newAABB.minimums.y > otherAABB.maximums.y) continue;
        if (newAABB.maximums.y < otherAABB.minimums.y) continue;
        if (newAABB.minimums.z > otherAABB.maximums.z) continue;
        if (newAABB.maximums.z < otherAABB.minimums.z) continue;

        // Check if we began colliding on the x axis this frame.
        if ((startAABB.minimums.x > otherAABB.maximums.x || startAABB.maximums.x < otherAABB.minimums.x)
            && !(newAABB.minimums.x > otherAABB.maximums.x || newAABB.maximums.x < otherAABB.minimums.x))
        {
            // Handle the bounce.
            if (startAABB.minimums.x > newAABB.minimums.x)
                SetLocalPosition(GetLocalPosition() + XMVectorSet(2.0f * (otherAABB.maximums.x - newAABB.minimums.x), 0.0f, 0.0f, 0.0f));
            else
                SetLocalPosition(GetLocalPosition() - XMVectorSet(2.0f * (newAABB.maximums.x - otherAABB.minimums.x), 0.0f, 0.0f, 0.0f));
            XMStoreFloat3(&m_speed, XMLoadFloat3(&m_speed) * XMVectorSet(-0.8f, 0.8f, 0.8f, 0.8f));
        }

        // Check if we began colliding on the y axis this frame.
        if ((startAABB.minimums.y > otherAABB.maximums.y || startAABB.maximums.y < otherAABB.minimums.y)
            && !(newAABB.minimums.y > otherAABB.maximums.y || newAABB.maximums.y < otherAABB.minimums.y))
        {
            // Handle the bounce.
            if (startAABB.minimums.y > newAABB.minimums.y) 
                SetLocalPosition(GetLocalPosition() + XMVectorSet(0.0f, 2.0f * (otherAABB.maximums.y - newAABB.minimums.y), 0.0f, 0.0f));
            else
                SetLocalPosition(GetLocalPosition() - XMVectorSet(0.0f, 2.0f * (newAABB.maximums.y - otherAABB.minimums.y), 0.0f, 0.0f));
            XMStoreFloat3(&m_speed, XMLoadFloat3(&m_speed) * XMVectorSet(0.8f, -0.8f, 0.8f, 0.8f));

            // Kill our velocity if it gets too slow.
            if (abs(m_speed.x) < 0.1f)
            {
                m_speed.x = 0.0f;
                m_acceleration.x = 0.0f;
            }
            if (abs(m_speed.y) < 0.1f)
            {
                m_speed.y = 0.0f;
                m_acceleration.y = 0.0f;
            }
            if (abs(m_speed.z) < 0.1f)
            {
                m_speed.z = 0.0f;
                m_acceleration.z = 0.0f;
            }
        }

        // Check if we began colliding on the z axis this frame.
        if ((startAABB.minimums.z > otherAABB.maximums.z || startAABB.maximums.z < otherAABB.minimums.z)
            && !(newAABB.minimums.z > otherAABB.maximums.z || newAABB.maximums.z < otherAABB.minimums.z))
        {
            // Handle the bounce.
            if (startAABB.minimums.z > newAABB.minimums.z)
                SetLocalPosition(GetLocalPosition() + XMVectorSet(0.0f, 0.0f, 2.0f * (otherAABB.maximums.z - newAABB.minimums.z), 0.0f));
            else
                SetLocalPosition(GetLocalPosition() - XMVectorSet(0.0f, 0.0f, 2.0f * (newAABB.maximums.z - otherAABB.minimums.z), 0.0f));
            XMStoreFloat3(&m_speed, XMLoadFloat3(&m_speed) * XMVectorSet(0.8f, 0.8f, -0.8f, 0.8f));
        }
    }
}

std::list<std::shared_ptr<AspenRenderer::GameObjects::ModelGameObject>> AspenRenderer::GameObjects::ModelGameObject::GetPhysicsObjects()
{
    // Create a list containing ourselves.
    std::list<std::shared_ptr<ModelGameObject>> objects{ shared_from_this() };
    for (const auto& child : m_children)
    {
        // Add the lists our children return to our list.
        objects.splice(objects.end(), child->GetPhysicsObjects());
    }
    return objects;
}
