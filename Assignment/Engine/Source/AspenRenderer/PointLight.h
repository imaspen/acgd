#pragma once

namespace AspenRenderer
{
    class PointLight
    {
    public:
        enum class ShadowDirection
        {
            NegativeX,
            PositiveX,
            NegativeY,
            PositiveY,
            NegativeZ,
            PositiveZ
        };

        struct LightBufferType
        {
            DirectX::XMFLOAT3 position;
            float specularHardness;
            DirectX::XMFLOAT3 ambientColor;
            float lightCount;
            DirectX::XMFLOAT3 diffuseColor;
            float diffusePower;
            DirectX::XMFLOAT3 specularColor;
            float specularPower;
        };

        struct LightMatrixBufferType
        {
            DirectX::XMMATRIX viewMatrix;
            DirectX::XMMATRIX projectionMatrix;
        };

        explicit PointLight(ID3D11Device& device);
        PointLight(ID3D11Device& device, const DirectX::XMFLOAT3& position, const DirectX::XMFLOAT3& ambientColor, const DirectX::XMFLOAT3& diffuseColor,
            const DirectX::XMFLOAT3& specularColor, float diffusePower, float specularPower, float specularHardness, ShadowDirection shadowDirection);

        LightBufferType AsBuffer(float lightCount) const noexcept;

        DirectX::XMMATRIX GetViewMatrix() const;
        static DirectX::XMMATRIX GetProjectionMatrix();
        void SetRenderTarget(ID3D11DeviceContext& deviceContext);
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> GetDepthMap() const noexcept { return m_shaderResource; }

        DirectX::XMFLOAT3 m_position, m_ambientColor;
        DirectX::XMFLOAT3 m_diffuseColor, m_specularColor;
        float m_diffusePower, m_specularPower, m_specularHardness;

    private:
        static constexpr int GetViewportSize() { return 1024; }

        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_texture{};
        Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTarget{};
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResource{};
        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_depthStencilBuffer{};
        Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView{};

        D3D11_VIEWPORT m_viewport{
            0.0f, 0.0f, static_cast<float>(GetViewportSize()), static_cast<float>(GetViewportSize()), 0.0f, 1.0f
        };

        ShadowDirection m_shadowDirection;
    };
}
