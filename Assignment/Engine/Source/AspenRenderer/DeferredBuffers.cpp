#include "pch.h"
#include "DeferredBuffers.h"

using namespace DirectX;

AspenRenderer::DeferredBuffers::DeferredBuffers(std::shared_ptr<D3DClass> d3d, const int textureWidth, const int textureHeight) :
    m_textureWidth(textureWidth),
    m_textureHeight(textureHeight),
    m_d3d(std::move(d3d)),
    m_textures(BUFFER_COUNT),
    m_renderTargetViews(BUFFER_COUNT),
    m_shaderResourceViews(BUFFER_COUNT),
    m_viewport{ 0.0f, 0.0f, static_cast<float>(textureWidth), static_cast<float>(textureHeight), 0.0f, 1.0f }
{
    auto& device = m_d3d->GetDevice();

    // Setup the texture description.
    D3D11_TEXTURE2D_DESC textureDesc;
    ZeroMemory(&textureDesc, sizeof textureDesc);
    textureDesc.Width = m_textureWidth;
    textureDesc.Height = m_textureHeight;
    textureDesc.MipLevels = 1;
    textureDesc.ArraySize = 1;
    textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    textureDesc.SampleDesc.Count = 1;
    textureDesc.Usage = D3D11_USAGE_DEFAULT;
    textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
    textureDesc.CPUAccessFlags = 0;
    textureDesc.MiscFlags = 0;

    // Setup the render target view description.
    D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
    ZeroMemory(&renderTargetViewDesc, sizeof renderTargetViewDesc);
    renderTargetViewDesc.Format = textureDesc.Format;
    renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    renderTargetViewDesc.Texture2D.MipSlice = 0;

    // Setup the description of the shader resource view.
    D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
    ZeroMemory(&shaderResourceViewDesc, sizeof shaderResourceViewDesc);
    shaderResourceViewDesc.Format = textureDesc.Format;
    shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
    shaderResourceViewDesc.Texture2D.MipLevels = 1;

    // Create a texture, render target view, and shader resource view for each buffer.
    for (auto i = 0; i < BUFFER_COUNT; ++i)
    {
        auto& texture = m_textures.at(i);

        DX::ThrowIfFailed(device.CreateTexture2D(&textureDesc, nullptr, texture.GetAddressOf()));

        DX::ThrowIfFailed(device.CreateRenderTargetView(texture.Get(), &renderTargetViewDesc, m_renderTargetViews.at(i).ReleaseAndGetAddressOf()));
        
        DX::ThrowIfFailed(device.CreateShaderResourceView(texture.Get(), &shaderResourceViewDesc, m_shaderResourceViews.at(i).ReleaseAndGetAddressOf()));
    }

    // Set up the depth buffer.
    D3D11_TEXTURE2D_DESC depthBufferDesc;
    ZeroMemory(&depthBufferDesc, sizeof depthBufferDesc);
    depthBufferDesc.Width = m_textureWidth;
    depthBufferDesc.Height = m_textureHeight;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    DX::ThrowIfFailed(device.CreateTexture2D(&depthBufferDesc, nullptr, m_depthStencilBuffer.ReleaseAndGetAddressOf()));

    // Set up the depth stencil view description.
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    ZeroMemory(&depthStencilViewDesc, sizeof depthStencilViewDesc);
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    DX::ThrowIfFailed(device.CreateDepthStencilView(m_depthStencilBuffer.Get(), &depthStencilViewDesc, m_depthStencilView.ReleaseAndGetAddressOf()));
}

void AspenRenderer::DeferredBuffers::SetRenderTargets()
{
    // Set the g-buffers as the render targets.
    std::vector<ID3D11RenderTargetView*> views;
    views.reserve(BUFFER_COUNT);
    for (const auto& view : m_renderTargetViews) views.push_back(view.Get());

    m_d3d->GetDeviceContext().RSSetViewports(1, &m_viewport);
    m_d3d->GetDeviceContext().OMSetRenderTargets(BUFFER_COUNT, views.data(), m_depthStencilView.Get());
}

void AspenRenderer::DeferredBuffers::ClearRenderTargets(const float red, const float green, const float blue, const float alpha)
{
    // Clear each g-buffers and the depth stencil.
    std::vector<float> color = { red, green, blue, alpha };

    for (const auto& view : m_renderTargetViews)
        m_d3d->GetDeviceContext().ClearRenderTargetView(view.Get(), color.data());

    m_d3d->GetDeviceContext().ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}

Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& AspenRenderer::DeferredBuffers::GetDepthStencil() noexcept
{
    return m_depthStencilView;
}

std::vector<ID3D11ShaderResourceView*> AspenRenderer::DeferredBuffers::GetRenderTargets()
{
    // Get a vector filled with the g-buffers shader resource views.
    std::vector<ID3D11ShaderResourceView*> views;
    views.reserve(BUFFER_COUNT);
    for (const auto& view : m_shaderResourceViews)
    {
        views.push_back(view.Get());
    }
    return views;
}
