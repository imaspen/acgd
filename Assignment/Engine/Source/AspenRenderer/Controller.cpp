#include "pch.h"
#include "Controller.h"

#include "GameObjects/ModelGameObject.h"

using namespace DirectX;

void AspenRenderer::Controller::Tick(const std::list<std::shared_ptr<GameObjects::ModelGameObject>>& physicsObjects, const float deltaTime)
{
    // Update our rotation based on the controller interface.
    const auto minRotation = DirectX::XMVectorSet(60.0f, 90.0f, 0.0f, 0.0f);
    const auto maxRotation = DirectX::XMVectorSet(-60.0f, -90.0f, 0.0f, 0.0f);
    const auto offset = DirectX::XMVectorSet(20.0f, 0.0f, 0.0f, 0.0f);
    //const auto currentRotation = m_camera->GetRotationInDegrees();
    const auto controllerRotation = m_ci->getRotation();
    //auto rotation = currentRotation + (controllerRotation * 0.5f);
    auto rotation = controllerRotation;
    rotation = DirectX::XMVectorMin(rotation, minRotation);
    rotation = DirectX::XMVectorMax(rotation, maxRotation);
    rotation += offset;
    rotation *= 180;
    m_camera->SetRotation(rotation);

    // Get the input and store it in a normalized vector for movement.
    if (m_input->IsRReleased()) {
        m_ci->m_resetHomeFlag = true;
    }
    XMFLOAT3 input(0.0f, 0.0f, 0.0f);
    if (m_input->IsWPressed()) input.z += 1.0f;
    if (m_input->IsSPressed()) input.z -= 1.0f;
    if (m_input->IsAPressed()) input.x -= 1.0f;
    if (m_input->IsDPressed()) input.x += 1.0f;
    const auto normalizedInput = XMVector3Transform(
        XMVector3Normalize(XMLoadFloat3(&input)),
        XMMatrixRotationY(XMConvertToRadians(0))
    );

    // Get our current speed and get our acceleration for the frame.
    auto speed = XMLoadFloat3(&m_speed);
    auto acceleration = 50.0f * normalizedInput;

    // If we're not moving, or nearly not moving. Kill the speed and exit.
    if (XMVector3NearEqual(speed, XMVectorZero(), XMVectorSet(0.1f, 0.1f, 0.1f, 0.1f))
        && XMVector3NearEqual(acceleration, XMVectorZero(), XMVectorSet(0.01f, 0.01f, 0.01f, 0.01f)))
    {
        XMStoreFloat3(&m_speed, XMVectorSet(0.0f, 0.0f, 0.0f, 0.0F));
        return;
    }

    // Get our AABB before the move.
    const auto startAABB = GetAABB();

    // Get our movement direction and apply friction.
    const auto movementDirection = XMVector3Normalize(XMLoadFloat3(&m_speed));
    acceleration -= 25.0f * movementDirection;

    // Update our speed based on acceleration and clamp it to 5m/s.
    speed += acceleration * deltaTime;
    speed = XMVector3ClampLength(speed, 0, 5.0f);
    XMStoreFloat3(&m_speed, speed);

    // Update our position based on speed. 
    SetPosition(m_camera->GetPosition() + deltaTime * speed);

    // Get our AABB after the move. 
    const auto endAABB = GetAABB();

    // Look for collisions in the physics objects vector.
    for (const auto& object : physicsObjects)
    {
        // Only collide with static objects.
        if (object->m_physicsType != GameObjects::ModelGameObject::PhysicsType::Static) continue;

        // Get the other objects AABB.
        const auto otherAABB = object->GetAABB();

        // Check we collide in every direction.
        if (startAABB.minimums.x > otherAABB.maximums.x && endAABB.minimums.x > otherAABB.maximums.x) continue;
        if (startAABB.maximums.x < otherAABB.minimums.x && endAABB.maximums.x < otherAABB.minimums.x) continue;
        if (startAABB.minimums.y > otherAABB.maximums.y && endAABB.minimums.y > otherAABB.maximums.y) continue;
        if (startAABB.maximums.y < otherAABB.minimums.y && endAABB.maximums.y < otherAABB.minimums.y) continue;
        if (startAABB.minimums.z > otherAABB.maximums.z && endAABB.minimums.z > otherAABB.maximums.z) continue;
        if (startAABB.maximums.z < otherAABB.minimums.z && endAABB.maximums.z < otherAABB.minimums.z) continue;

        // Kill our speed in the colliding direction.
        if (startAABB.minimums.x > otherAABB.maximums.x)
        {
            SetPosition(XMVectorSetX(m_camera->GetPosition(), otherAABB.maximums.x + m_aabbRadius));
            m_speed.x = 0.0f;
        }
        else if (startAABB.maximums.x < otherAABB.minimums.x)
        {
            SetPosition(XMVectorSetX(m_camera->GetPosition(), otherAABB.minimums.x - m_aabbRadius));
            m_speed.x = 0.0f;
        }
        if (startAABB.minimums.z > otherAABB.maximums.z)
        {
            SetPosition(XMVectorSetZ(m_camera->GetPosition(), otherAABB.maximums.z + m_aabbRadius));
            m_speed.z = 0.0f;
        }
        else if (startAABB.maximums.z < otherAABB.minimums.z)
        {
            SetPosition(XMVectorSetZ(m_camera->GetPosition(), otherAABB.minimums.z - m_aabbRadius));
            m_speed.z = 0.0f;
        }
    }
}

AspenRenderer::AABB AspenRenderer::Controller::GetAABB() const
{
    // If the cache isn't valid, update it.
    if (!m_aabbCacheValid)
        m_aabbCache = m_aabb.Transform(XMMatrixTranslationFromVector(m_camera->GetPosition()));

    return m_aabbCache;
}
