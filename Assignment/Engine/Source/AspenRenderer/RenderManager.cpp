#include "pch.h"
#include "RenderManager.h"

#include "Camera.h"
#include "DeferredBuffers.h"
#include "Shaders/MirrorShader.h"
#include "Shaders/Shader.h"

using namespace AspenRenderer::Models;
using namespace AspenRenderer::Shaders;
using namespace DirectX;
using Microsoft::WRL::ComPtr;

AspenRenderer::RenderManager::RenderManager(std::shared_ptr<D3DClass> d3d, std::shared_ptr<Camera> camera, const int screenWidth, const int screenHeight) :
    m_d3d(std::move(d3d)),
    m_camera(std::move(camera))
{
    m_deferredBuffers = std::make_shared<DeferredBuffers>(m_d3d, screenWidth, screenHeight);
    m_depthShader = std::make_unique<DepthShader>(m_d3d);
    m_deferredAlbedoShader = std::make_unique<TextureShader>(m_d3d, "DeferredAlbedoVertexShader.cso", "DeferredAlbedoPixelShader.cso");
    m_deferredBumpMapShader = std::make_unique<DeferredBumpMapShader>(m_d3d);
    m_deferredPBRShader = std::make_unique<DeferredPBRShader>(m_d3d);
    m_deferredLightShader = std::make_unique<DeferredLightShader>(m_d3d);
    m_fireShader = std::make_unique<FireShader>(m_d3d);
    m_mirrorShader = std::make_unique<MirrorShader>(m_d3d);
    m_windowModel = std::make_unique<WindowModel>(m_d3d->GetDevice(), screenWidth, screenHeight);
    m_lights = {
        std::make_shared<PointLight>(
            m_d3d->GetDevice(),
            XMFLOAT3(0.0f, 3.0f, 0.0f),
            XMFLOAT3(0.125f, 0.085f, 0.045f),
            XMFLOAT3(1.0f, 0.8f, 0.6f),
            XMFLOAT3(1.0f, 0.8f, 0.6f),
            50.0f, 25.0f, 16.0f, PointLight::ShadowDirection::NegativeY
        ),
        std::make_shared<PointLight>(
            m_d3d->GetDevice(),
            XMFLOAT3(0.0f, 0.5f, 3.25f),
            XMFLOAT3(0.125f, 0.085f, 0.045f),
            XMFLOAT3(1.0f, 0.6f, 0.2f),
            XMFLOAT3(1.0f, 0.6f, 0.2f),
            100.0f, 100.0f, 16.0f, PointLight::ShadowDirection::NegativeZ
        )
    };
}

void AspenRenderer::RenderManager::RegisterAlbedoModel(std::shared_ptr<Model> model,
    const XMMATRIX& transform, ComPtr<ID3D11ShaderResourceView> albedoTexture)
{
    // Put model into the albedo queue to be rendered
    m_albedoQueue.push_back({ std::move(model), transform, std::move(albedoTexture) });
}

void AspenRenderer::RenderManager::RegisterBumpMapModel(std::shared_ptr<Model> model, const XMMATRIX& transform,
    ComPtr<ID3D11ShaderResourceView> albedoTexture, ComPtr<ID3D11ShaderResourceView> bumpMap)
{
    // Put model into the bump map queue to be rendered
    m_bumpMapQueue.push_back({ std::move(model), transform, std::move(albedoTexture), std::move(bumpMap) });
}

void AspenRenderer::RenderManager::RegisterGlossMapModel(std::shared_ptr<Model> model, const XMMATRIX& transform,
    ComPtr<ID3D11ShaderResourceView> albedoTexture, ComPtr<ID3D11ShaderResourceView> glossMap)
{
    // Put model into the gloss map queue to be rendered
    m_glossMapQueue.push_back({ std::move(model), transform, std::move(albedoTexture), std::move(glossMap) });
}

void AspenRenderer::RenderManager::RegisterPBRModel(std::shared_ptr<Model> model, const XMMATRIX& transform,
    ComPtr<ID3D11ShaderResourceView> albedoTexture, ComPtr<ID3D11ShaderResourceView> bumpMap,
    ComPtr<ID3D11ShaderResourceView> glossMap)
{
    // Put model into the pbr queue to be rendered
    m_pbrQueue.push_back({ std::move(model), transform, std::move(albedoTexture), std::move(bumpMap), std::move(glossMap) });
}

void AspenRenderer::RenderManager::RegisterFireModel(std::shared_ptr<FireModel> model, const float frameTime, const XMMATRIX& transform,
    ComPtr<ID3D11ShaderResourceView> albedoTexture,
    ComPtr<ID3D11ShaderResourceView> alphaMap,
    ComPtr<ID3D11ShaderResourceView> noiseMap)
{
    // Put model into the fire queue to be rendered
    m_fireQueue.push_back({ std::move(model), frameTime, transform, std::move(albedoTexture), std::move(alphaMap), std::move(noiseMap) });
}

void AspenRenderer::RenderManager::RegisterMirrorModel(std::shared_ptr<Model> model, const XMMATRIX& transform, const XMMATRIX& viewMatrix,
    std::shared_ptr<DeferredBuffers> buffers, ComPtr<ID3D11RenderTargetView> renderTarget, ComPtr<ID3D11DepthStencilView> depthStencil, 
    ComPtr<ID3D11ShaderResourceView> colorTexture, ComPtr<ID3D11ShaderResourceView> reflectionTexture)
{
    // Put model into the mirror queue to be rendered
    m_mirrorQueue.push_back({ std::move(model), transform, viewMatrix, std::move(buffers),
        std::move(renderTarget), std::move(depthStencil), std::move(colorTexture), std::move(reflectionTexture) });
}

void AspenRenderer::RenderManager::Render(const unsigned int isolatedLight, const float shaderMode)
{
    // Get the wold to screen space matrices.
    const auto& viewMatrix = m_camera->GetViewMatrix();
    const auto& projectionMatrix = m_d3d->GetProjectionMatrix();

    // Render the scene.
    m_d3d->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

    RenderShadowDepthMaps();

    RenderDeferredModels(*m_deferredBuffers, viewMatrix, projectionMatrix);

    RenderGBuffers(isolatedLight, shaderMode);

    RenderFires(viewMatrix, projectionMatrix);

    RenderMirrors(viewMatrix, projectionMatrix, shaderMode);

    m_d3d->EndScene();

    // Clear the model queues.
    m_albedoQueue.clear();
    m_bumpMapQueue.clear();
    m_pbrQueue.clear();
    m_fireQueue.clear();
    m_mirrorQueue.clear();
}

void AspenRenderer::RenderManager::RenderShadowDepthMaps()
{
    // For each light.
    for (const auto& light : m_lights)
    {
        // Get view and projection matrices.
        const auto lightViewMatrix = light->GetViewMatrix();
        const auto lightProjectionMatrix = light->GetProjectionMatrix();

        // Set the shadow map as the render target.
        light->SetRenderTarget(m_d3d->GetDeviceContext());

        // Render the models into the depth texture.
        for (const auto& model : m_albedoQueue)
        {
            model.model->RenderBuffers(m_d3d->GetDeviceContext());
            m_depthShader->SetShaderParameters(model.model->GetTransform() * model.transform, lightViewMatrix, lightProjectionMatrix);
            m_depthShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
        }
        for (const auto& model : m_bumpMapQueue)
        {
            model.model->RenderBuffers(m_d3d->GetDeviceContext());
            m_depthShader->SetShaderParameters(model.model->GetTransform() * model.transform, lightViewMatrix, lightProjectionMatrix);
            m_depthShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
        }
        for (const auto& model : m_pbrQueue)
        {
            model.model->RenderBuffers(m_d3d->GetDeviceContext());
            m_depthShader->SetShaderParameters(model.model->GetTransform() * model.transform, lightViewMatrix, lightProjectionMatrix);
            m_depthShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
        }
    }
}

void AspenRenderer::RenderManager::RenderDeferredModels(DeferredBuffers& buffers, const XMMATRIX& viewMatrix, const XMMATRIX& projectionMatrix)
{
    // Bind and clear the g-buffers.
    buffers.SetRenderTargets();
    buffers.ClearRenderTargets(0.0f, 0.0f, 0.0f, 1.0f);

    // Render models int the g-buffers.
    for (const auto& model : m_albedoQueue)
    {
        model.model->RenderBuffers(m_d3d->GetDeviceContext());
        m_deferredAlbedoShader->SetShaderParameters(model.model->GetTransform() * model.transform, viewMatrix, projectionMatrix, model.albedoTexture.Get());
        m_deferredAlbedoShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
    }
    for (const auto& model : m_bumpMapQueue)
    {
        model.model->RenderBuffers(m_d3d->GetDeviceContext());
        m_deferredBumpMapShader->SetShaderParameters(model.model->GetTransform() * model.transform, viewMatrix, projectionMatrix, model.albedoTexture.Get(), model.bumpMap.Get());
        m_deferredBumpMapShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
    }
    for (const auto& model : m_pbrQueue)
    {
        model.model->RenderBuffers(m_d3d->GetDeviceContext());
        m_deferredPBRShader->SetShaderParameters(model.model->GetTransform() * model.transform, viewMatrix, projectionMatrix, model.albedoTexture.Get(), model.bumpMap.Get(), model.glossMap.Get());
        m_deferredPBRShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
    }
}

void AspenRenderer::RenderManager::RenderGBuffers(const unsigned int isolatedLight, const float shaderMode)
{
    // Set the back buffer to be active and clear it.
    m_d3d->SetBackBufferRenderTarget(m_deferredBuffers->GetDepthStencil());
    m_d3d->EnableColorBlending();

    // Disable the z-buffer to allow forward rendering after the g-buffer pass.
    m_d3d->TurnZBufferOff();

#if _DEBUG
    // If debugging and isolating a light, render the scene only lit with that light.
    if (isolatedLight >= 1 && isolatedLight <= m_lights.size())
    {
        const auto& light = m_lights.at(isolatedLight - 1);
        m_deferredLightShader->SetShaderParameters(m_windowModel->GetTransform(), m_camera->GetBaseViewMatrix(), m_d3d->GetOrthoMatrix(),
            m_camera->GetPosition(), shaderMode, *light, 1, *m_deferredBuffers);
        m_windowModel->RenderBuffers(m_d3d->GetDeviceContext());
        m_deferredLightShader->RenderShader(m_windowModel->m_indexCount);
    }
    else
#endif
        // Render the buffers for each light.
        for (const auto& light : m_lights)
        {
            m_deferredLightShader->SetShaderParameters(m_windowModel->GetTransform(), m_camera->GetBaseViewMatrix(), m_d3d->GetOrthoMatrix(),
                m_camera->GetPosition(), shaderMode, *light, m_lights.size(), *m_deferredBuffers);
            m_windowModel->RenderBuffers(m_d3d->GetDeviceContext());
            m_deferredLightShader->RenderShader(m_windowModel->m_indexCount);

#if _DEBUG
            // If we're in a debug shader mode, only render one light.
            if (shaderMode > 0.0f && shaderMode < 6.0f) break;
#endif
        }

    m_d3d->TurnZBufferOn();
}

void AspenRenderer::RenderManager::RenderFires(const XMMATRIX& viewMatrix, const XMMATRIX& projectionMatrix)
{
    // Enable alpha blending so we can fade the edge of the flames.
    m_d3d->EnableAlphaBlending();

    // Render the fire models.
    for (const auto& model : m_fireQueue)
    {
        model.model->RenderBuffers(m_d3d->GetDeviceContext());
        m_fireShader->SetShaderParameters(model.model->GetTransform() * model.transform, viewMatrix, projectionMatrix, model.frameTime,
            model.albedoTexture.Get(), model.alphaMap.Get(), model.noiseMap.Get());
        m_fireShader->RenderShader(model.model->m_vertexCount, model.model->m_instanceCount);
    }

    m_d3d->DisableAlphaBlending();
}

void AspenRenderer::RenderManager::RenderMirrors(const XMMATRIX& viewMatrix, const XMMATRIX& projectionMatrix, const float shaderMode)
{
    // For each mirror
    for (const auto& mirror : m_mirrorQueue)
    {
        // Render the scene into g-buffers from the mirrored perspective.
        RenderDeferredModels(*mirror.buffers, mirror.viewMatrix, projectionMatrix);

        // Render the g-buffers into the reflection texture.
        const float color[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
        m_d3d->GetDeviceContext().OMSetRenderTargets(1, mirror.renderTarget.GetAddressOf(), mirror.depthStencil.Get());
        m_d3d->GetDeviceContext().ClearRenderTargetView(mirror.renderTarget.Get(), static_cast<const float*>(color));
        m_d3d->GetDeviceContext().ClearDepthStencilView(mirror.depthStencil.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
        m_d3d->EnableColorBlending();

        for (const auto& light : m_lights)
        {
            m_deferredLightShader->SetShaderParameters(m_windowModel->GetTransform(), m_camera->GetBaseViewMatrix(), m_d3d->GetOrthoMatrix(),
                m_camera->GetPosition(), shaderMode, *light, m_lights.size(), *mirror.buffers);
            m_windowModel->RenderBuffers(m_d3d->GetDeviceContext());
            m_deferredLightShader->RenderShader(m_windowModel->m_indexCount);
        }

        m_d3d->DisableAlphaBlending();

        // Render the mirror onto the back buffer.
        m_d3d->SetBackBufferRenderTarget(m_deferredBuffers->GetDepthStencil());

        mirror.model->RenderBuffers(m_d3d->GetDeviceContext());
        m_mirrorShader->SetShaderParameters(mirror.model->GetTransform() * mirror.transform, viewMatrix, projectionMatrix, mirror.viewMatrix, mirror.colorTexture.Get(), mirror.reflectionTexture.Get());
        m_mirrorShader->RenderShader(mirror.model->m_vertexCount, mirror.model->m_instanceCount);
    }
}
