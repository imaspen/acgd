#pragma once

namespace AspenRenderer
{
    class Camera
    {
    public:
        explicit Camera(
            const DirectX::XMVECTOR& position = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
            const DirectX::XMVECTOR& rotation = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)
        )
        {
            DirectX::XMStoreFloat3(&m_position, position);
            DirectX::XMStoreFloat3(&m_rotation, rotation);
            m_rotation.x = DirectX::XMConvertToRadians(m_rotation.x);
            m_rotation.y = DirectX::XMConvertToRadians(m_rotation.y);
            m_rotation.z = DirectX::XMConvertToRadians(m_rotation.z);
        }

        DirectX::XMMATRIX GetViewMatrix() const;
        DirectX::XMMATRIX GetBaseViewMatrix() const { return XMLoadFloat4x4(&m_baseViewMatrix); }

        DirectX::XMVECTOR GetPosition() const { return XMLoadFloat3(&m_position); }
        void SetPosition(const DirectX::XMVECTOR& position);

        DirectX::XMVECTOR GetRotation() const { return XMLoadFloat3(&m_rotation); }
        DirectX::XMVECTOR GetRotationInDegrees() const;
        void SetRotation(const DirectX::XMVECTOR& rotation);

    private:
        DirectX::XMFLOAT4X4 m_baseViewMatrix{
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 1.0f
        };
        DirectX::XMFLOAT3 m_position{0.0f, 1.5f, 0.0f}, m_rotation{};

        mutable DirectX::XMFLOAT4X4 m_viewMatrixCache{};
        mutable bool m_viewMatrixCacheValid = false;
    };
}
