#pragma once

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

#include <dxgi.h>

class D3DClass
{
public:
    D3DClass(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen,
        float screenDepth, float screenNear);
    D3DClass(const D3DClass&) = delete;
    D3DClass(D3DClass&&) = delete;
    ~D3DClass();
    D3DClass& operator= (const D3DClass&) = delete;
    D3DClass& operator= (D3DClass&&) = delete;

    void BeginScene(float, float, float, float) const;
    void EndScene() const;

    ID3D11Device& GetDevice() const noexcept { return *m_device.Get(); }
    ID3D11DeviceContext& GetDeviceContext() const noexcept { return *m_deviceContext.Get(); }
    ID3D11DepthStencilView& GetDepthStencilView() const noexcept { return *m_depthStencilView.Get(); }

    DirectX::XMMATRIX GetProjectionMatrix() const { return XMLoadFloat4x4(&m_projectionMatrix); }
    DirectX::XMMATRIX GetWorldMatrix() const { return XMLoadFloat4x4(&m_worldMatrix); }
    DirectX::XMMATRIX GetOrthoMatrix() const { return XMLoadFloat4x4(&m_orthoMatrix); }

    void GetVideoCardInfo(char*, int&) const;

    void SetBackBufferRenderTarget(const Microsoft::WRL::ComPtr<ID3D11DepthStencilView>& depthStencil = nullptr);
    void TurnZBufferOn() const;
    void TurnZBufferOff() const;
    void ResetViewport() const;

    void EnableColorBlending() const;
    void EnableAlphaBlending() const;
    void DisableAlphaBlending() const;

private:
    bool m_vsyncEnabled;
    int m_videoCardMemory;
    char m_videoCardDescription[128]{};
    D3D11_VIEWPORT m_viewport{};

    Microsoft::WRL::ComPtr<IDXGISwapChain> m_swapChain;
    Microsoft::WRL::ComPtr<ID3D11Device> m_device;
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> m_deviceContext;
    Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> m_depthStencilBuffer;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_depthStencilState;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilState> m_depthDisabledStencilState;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView;
    Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_rasterState;
    Microsoft::WRL::ComPtr<ID3D11BlendState> m_blendEnabledState;
    Microsoft::WRL::ComPtr<ID3D11BlendState> m_blendDisabledState;
    Microsoft::WRL::ComPtr<ID3D11BlendState> m_colorAddBlendState;

    DirectX::XMFLOAT4X4 m_projectionMatrix{};
    DirectX::XMFLOAT4X4 m_worldMatrix{};
    DirectX::XMFLOAT4X4 m_orthoMatrix{};
};
