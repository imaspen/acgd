#include "pch.h"
#include "timerclass.h"

TimerClass::TimerClass() noexcept
{
    // Check to see if this system supports high performance timers.
    QueryPerformanceFrequency(&m_frequency);
    m_ticksPerMs = m_frequency.QuadPart / 1000.0f;
    QueryPerformanceCounter(&m_startTime);
}

void TimerClass::Frame() noexcept
{
	// Query the current time.
	LARGE_INTEGER currentTime = {};
	QueryPerformanceCounter(&currentTime);

    const auto timeDifference = currentTime.QuadPart - m_startTime.QuadPart;

	// Calculate the frame time by the time difference over the timer speed resolution.
	m_frameTime = static_cast<float>(timeDifference) / m_ticksPerMs;

	// Restart the timer.
	m_startTime = currentTime;
}

float TimerClass::GetTime() const noexcept
{
	return m_frameTime / 1000.0f;
}
