#pragma once

class ControllerInterface {
public:
	ControllerInterface();

	DirectX::XMVECTOR getRotation() const;
	void setRotation(const DirectX::XMVECTOR newRotation);

	std::atomic_bool m_resetHomeFlag;

private:
	mutable std::mutex m_rotationLock{};

	std::unique_ptr<DirectX::XMFLOAT2> m_rotation;
};
