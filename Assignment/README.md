# Aspen Thomspon - CIS2203 Real Time Graphics Coursework.
The project has been set to use the latest released Windows 10 SDK at the time of writing, 10.0.18362.0, and MSVC v142 rather than the original outdated SDK and build tools.

## Controls:
- WASD: Movement
- Mouse: Camera look
- E: Throw ball
## Debug Mode Controls:
- R: Reset shader and light modes (Debug builds only)
- 1: Isolate world position
- 2: Isolate albedo
- 3: Isolate normals
- 4: Isolate specularity
- 5: Isolate shadow map
- 6: Isolate ambient + diffuse lighting
- 7: Isolate ambient + specular lighting
- 0: Reset shader mode
- Shift + 1: Isolate candle light
- Shift + 2: Isolate fire light
- Shift + 0: Show all lights

